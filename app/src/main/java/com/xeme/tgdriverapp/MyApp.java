package com.xeme.tgdriverapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.support.multidex.MultiDex;

import com.facebook.stetho.Stetho;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;
import com.orm.SugarApp;
import com.xeme.tgdriverapp.dagger.component.DaggerNetComponent;
import com.xeme.tgdriverapp.dagger.component.NetComponent;
import com.xeme.tgdriverapp.dagger.module.AppModule;
import com.xeme.tgdriverapp.dagger.module.NetModule;
import com.xeme.tgdriverapp.models.Simple_Response;
import com.xeme.tgdriverapp.models.User;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MySharedPreferences;
import com.xeme.tgdriverapp.utils.SlimApi;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.xeme.tgdriverapp.utils.MyHelper.Logwtf;

public class MyApp extends SugarApp {
    public static Typeface Typeface_Light = null;
    public static Typeface Typeface_Thin = null;
    @Inject
    Retrofit retrofit;
    private static MySharedPreferences MyPref;
    @Inject
    MySharedPreferences mMyPref;
    private NetComponent mNetComponent;

    public MyApp() {
    }

    public static void SaveUserProfile(User mUser) {
        MyPref.putInt(MyConsta.KEY_MYPID, mUser.did);
        MyPref.putString(MyConsta.KEY_FullName, mUser.name);
        MyPref.putString(MyConsta.KEY_NickName, mUser.nickname);
        MyPref.putString(MyConsta.KEY_Email, mUser.email);
        MyPref.putString(MyConsta.KEY_Phone, mUser.mobile);
        MyPref.putString(MyConsta.KEY_Address, mUser.address);
        MyPref.putString(MyConsta.KEY_City, mUser.city);
        MyPref.putString(MyConsta.KEY_State, mUser.state);
        MyPref.putString(MyConsta.KEY_PostalCode, mUser.pincode);
        MyPref.putString(MyConsta.KEY_ProofType, mUser.proofType);
        MyPref.putString(MyConsta.KEY_ProofNumber, mUser.proofNumber);
        MyPref.putString(MyConsta.KEY_UUID, mUser.apikey);
        MyPref.putString(MyConsta.KEY_CurrentBalance, String.valueOf(mUser.currentBalance));
       /* String Fname = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, mUser.firstName);
        String Lname = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, mUser.lastName);
        MyPref.putString(MyConsta.KEY_FullName, Fname + " " + Lname);*/

    }

    public static LatLng getLocationFromAddress(Context ctx, String strAddress) {


        Geocoder coder = new Geocoder(ctx);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;

    }
/*
    public static LatLng getLocationFromAddress2(Context ctx, String strAddress) {


        rx.Observable.just().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                Geocoder coder = new Geocoder(ctx);
                List<Address> address;
                LatLng p1 = null;
                try {
                    address = coder.getFromLocationName(strAddress, 5);
                    if (address == null) {
                        p1 = null;
                    }
                    else {
                        Address location = address.get(0);
                        location.getLatitude();
                        location.getLongitude();

                        p1 = new LatLng(location.getLatitude(), location.getLongitude());
                    }


                } catch (Exception ex) {

                    ex.printStackTrace();
                }

                return p1;
            }
        });



    }*/

    public static  void UpdateNotifToken(Retrofit retrofit, int PID, String UUID) {

        String Token = FirebaseInstanceId.getInstance().getToken();
        if (UUID.length() > 0 && PID > 0) {
            Call<Simple_Response> UPDATE_FBT = retrofit.create(SlimApi.class).PostUpdateFBToken(UUID, PID, Token);
            UPDATE_FBT.enqueue(new Callback<Simple_Response>() {
                @Override
                public void onResponse(Call<Simple_Response> call, Response<Simple_Response> response) {
                    if (response.code() == 200) {
                        if (response != null) {

                            if (response.body().error.equals(false)) {
                                MyPref.putString(MyConsta.KEY_FBTOKEN, Token);
                                Logwtf("Response On Success", "FBT Updated");
                            } else {

                                Logwtf("Response Fail", "FBT Not Updated");
                            }
                        }

                        } else if (response.code() == 201) {

                            Logwtf("Response Register", response.headers().toString() + "\n" + response.raw() + "\n Response msg" + response.body().message);


                        } else {
                            Logwtf("Response Register", response.headers().toString() + "\n" + response.raw() + "\n Response msg" + response.raw().toString());
                        }

                }

                @Override
                public void onFailure(Call<Simple_Response> call, Throwable t) {
                    Logwtf("onUpdated", "UID OR CID NULL" + UUID + " MID" + PID + " Fail Cause " + t.getCause());
                }
            });

        }
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Intent Internet = new Intent("com.Check.net");
        sendBroadcast(Internet);
        MyPref = MySharedPreferences.getInstance(getApplicationContext(), MyConsta.My_Pref);
        Stetho.initializeWithDefaults(this);
        mNetComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(MyConsta.URL_BASE))
                .build();

        this.getNetComponent().inject(this);
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }

    public void initTypeface() {
        Typeface_Light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/RobotoLight.ttf");
        Typeface_Thin = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/RobotoThin.ttf");

    }
}
