package com.xeme.tgdriverapp.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.xeme.tgdriverapp.MyApp;
import com.xeme.tgdriverapp.R;
import com.xeme.tgdriverapp.models.PastOrder;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MyHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class Adap_RideHistory extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_EMPTY_LIST_PLACEHOLDER = 0;
    private static final int VIEW_TYPE_OBJECT_VIEW = 1;

    private int pos;
    private String EmptyMsg = "No Record Found";
    private List<PastOrder> mValues = new ArrayList<>();
    private Context Ctx;
    private Typeface Typeface_Thin;
    private Typeface Typeface_Light;
    private AdapHomeHelper mHomeHelper;
    private SimpleDateFormat TimeFormat = MyConsta.mDateFormat;



    public Adap_RideHistory(List<PastOrder> mlist, Context ctx, AdapHomeHelper AdapHomeHelper, @Nullable String EmptyMsg) {
        mValues = mlist;
        mHomeHelper = AdapHomeHelper;
        TimeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        if (EmptyMsg != null) {
            this.EmptyMsg = EmptyMsg;
        }
        Ctx = ctx;
        this.Typeface_Light = MyApp.Typeface_Light;
        this.Typeface_Thin = MyApp.Typeface_Thin;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_EMPTY_LIST_PLACEHOLDER:
                View v1 = inflater.inflate(R.layout.list_item_empty, parent, false);
                // return view holder for your placeholder
                viewHolder = new EmptyViewHolder(v1);

                break;
            case VIEW_TYPE_OBJECT_VIEW:
                View v2 = inflater.inflate(R.layout.list_item_order_history                                                                                                             , parent, false);
                // return view holder for your normal list item
                viewHolder = new ViewHolder(v2);
                break;
        }

        return viewHolder;

    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);

        if (holder instanceof ViewHolder) {
            ViewHolder v1 = (ViewHolder) holder;
            YoYo.with(Techniques.FadeIn).duration(600).interpolate(new OvershootInterpolator()).playOn(v1.mView);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (mValues.isEmpty()) {
            return VIEW_TYPE_EMPTY_LIST_PLACEHOLDER;
        } else {
            return VIEW_TYPE_OBJECT_VIEW;
        }
    }

    private void configureNonEmptyView(ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.lihoOriginAddress.setText(holder.mItem .pickupArea);
        holder.lihoDestinationAddress.setText(holder.mItem .dropArea);
        holder.lihoPrice.setText(String.valueOf( holder.mItem .amount +" Rs."));
        holder.lihoTime.setText( String.valueOf("Not Available"));
        holder.lihoDistance.setText(  holder.mItem .distance);
        holder.lihoPickupTime.setText(holder.mItem .pickupTime + " "+holder.mItem .pickupDate);
        holder.lihoAcrbRate.setRating(Float.valueOf(holder.mItem.rating));

    }

  /*  @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }*/

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder vh2 = null;
        if (holder instanceof EmptyViewHolder) {
            EmptyViewHolder vh1 = (EmptyViewHolder) holder;
        }
        if (holder instanceof ViewHolder) {
            vh2 = (ViewHolder) holder;
            configureNonEmptyView(vh2,position);
        }


    }

    @Override
    public int getItemCount() {

        return mValues.size() > 0 ? mValues.size() : 1;
    }


    class EmptyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lle_empty_msg)
        AppCompatTextView lleEmptyMsg;

        private EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            lleEmptyMsg.setText(EmptyMsg);
            lleEmptyMsg.setCompoundDrawablePadding(MyHelper.dpToPx(5));
            // lleEmptyMsg.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_dart_board_black_24dp,0,0);


        }
    }



    public interface AdapHomeHelper {
      //  void AcceptReq(PastOrder SelectedData, int pos);

     /*   void IsSelectedOpp1(PastOrder SelectedData, int pos);

        void IsSelectedOpp2(PastOrder SelectedData, int pos);
   */ }

    class ViewHolder  extends RecyclerView.ViewHolder{
        private final View mView;
        private PastOrder mItem;
        @BindView(R.id.liho_origin_address)
        AppCompatTextView lihoOriginAddress;
        @BindView(R.id.liho_destination_address)
        AppCompatTextView lihoDestinationAddress;
        @BindView(R.id.liho_time)
        AppCompatTextView lihoTime;
        @BindView(R.id.liho_price)
        AppCompatTextView lihoPrice;
        @BindView(R.id.liho_distance)
        AppCompatTextView lihoDistance;
        @BindView(R.id.liho_pickup_time)
        AppCompatTextView lihoPickupTime;
        @BindView (R.id.liho_acrb_rate)
        AppCompatRatingBar lihoAcrbRate;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            mView = view;
            lihoAcrbRate.setClickable(false);
            lihoAcrbRate.setNumStars(5);
            lihoAcrbRate.setStepSize(1f);
        }
    }
}
