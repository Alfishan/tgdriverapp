package com.xeme.tgdriverapp.broadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.xeme.tgdriverapp.MyApp;
import com.xeme.tgdriverapp.models.NetworkStateChanged;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MySharedPreferences;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import static com.xeme.tgdriverapp.utils.MyHelper.Logwtf;


public class ConnectivityChecker extends BroadcastReceiver {


    @Inject
    MySharedPreferences mMyPref;
    public ConnectivityChecker() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        ((MyApp) context.getApplicationContext()).getNetComponent().inject(ConnectivityChecker.this);
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if(activeNetworkInfo == null){
            //no network
            EventBus.getDefault().post(new NetworkStateChanged(false));
            mMyPref.putBoolean(MyConsta.KEY_IsInternet,false);
            Logwtf("onReceive","activeNetworkInfo is null no internet ");
        } else {
            NetworkInfo.State state = activeNetworkInfo.getState();
            if (state== NetworkInfo.State.CONNECTED) {
                EventBus.getDefault().post(new NetworkStateChanged(true));
                mMyPref.putBoolean(MyConsta.KEY_IsInternet,true);
                Logwtf("onReceive","yey We Have Internet");
            }
            else {
                EventBus.getDefault().post(new NetworkStateChanged(false));
                mMyPref.putBoolean(MyConsta.KEY_IsInternet,false);
                Logwtf("onReceive","InterNet Gone :(");
            }

        }

    }
}
