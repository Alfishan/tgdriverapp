package com.xeme.tgdriverapp.dagger.component;


import com.xeme.tgdriverapp.MyApp;
import com.xeme.tgdriverapp.broadcastReceivers.ConnectivityChecker;
import com.xeme.tgdriverapp.broadcastReceivers.GPSChecker;
import com.xeme.tgdriverapp.dagger.module.AppModule;
import com.xeme.tgdriverapp.dagger.module.NetModule;
import com.xeme.tgdriverapp.services.MyFirebaseInstanceIDService;
import com.xeme.tgdriverapp.services.MyLocationService;
import com.xeme.tgdriverapp.services.MySyncService;
import com.xeme.tgdriverapp.ui.activity.HomeActivity;
import com.xeme.tgdriverapp.ui.activity.LoginActivity;
import com.xeme.tgdriverapp.ui.activity.RideHistoryActivity;
import com.xeme.tgdriverapp.ui.activity.RideRequestActivity;
import com.xeme.tgdriverapp.ui.activity.RideRequestActivityFragment;
import com.xeme.tgdriverapp.ui.activity.SplashActivity;
import com.xeme.tgdriverapp.ui.fragment.Frag_RR_StartStop;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by root on 23/9/16.
 */
@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    /*void inject(Activity_Fitbit activity);
    void inject(LoginActivity activity);
    void inject(Activity_Vitals activity);
    void inject(MySyncService activity);
    void inject(ActivityUserProfile activity);

    void inject(Frag_CheckIns fragment);
    void inject(Frag_Vitals fragment);
    void inject(Frag_Home fragment);
    void inject(Frag_Medications fragment);
    void inject(Frag_ProgressBar fragment);
    void inject(Frag_Appointments fragment);
    void inject(FragCmContacts fragment);
    void inject(Frag_CheckIns_Survay fragment);
    void inject(Frag_Vitals_Fitbit_Sync fragment);

    void inject(ChatSingleActivity activity);
    void inject(ChatMasterActivity activity);*/
    void inject(ConnectivityChecker activity);
    void inject(MyApp Application);
    void inject(Frag_RR_StartStop activity);
    void inject(GPSChecker activity);
    void inject(LoginActivity activity);
    void inject(SplashActivity activity);
    void inject(MyLocationService activity);
    void inject(HomeActivity activity);
    void inject(RideRequestActivity activity);
    void inject(RideRequestActivityFragment activity);
    void inject(MySyncService activity);
    void inject(MyFirebaseInstanceIDService activity);
    void inject(RideHistoryActivity activity);
  //  void inject(RideHistoryActivity activity);
}