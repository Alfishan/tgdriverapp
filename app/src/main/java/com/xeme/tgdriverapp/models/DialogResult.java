package com.xeme.tgdriverapp.models;

/**
 * Created by root on 20/10/16.
 */

public class DialogResult {
    public int getForWho() {
        return ForWho;
    }

    public boolean isResult() {
        return Result;
    }

    private int ForWho;
    private boolean Result;

    public DialogResult() {
    }

    public DialogResult(int forWho, boolean result) {
        ForWho = forWho;
        Result = result;
    }
}
