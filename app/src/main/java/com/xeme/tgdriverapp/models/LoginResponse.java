package com.xeme.tgdriverapp.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("error")
    @Expose
    public Boolean error;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("pid")
    @Expose
    public Integer pid;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("gender")
    @Expose
    public Integer gender;
    @SerializedName("dob")
    @Expose
    public String dob;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("postal_code")
    @Expose
    public Integer postalCode;
    @SerializedName("phoneone")
    @Expose
    public String phoneone;
    @SerializedName("phonetwo")
    @Expose
    public String phonetwo;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("medicalinsurance")
    @Expose
    public Integer medicalinsurance;
    @SerializedName("medicalinsurance_name")
    @Expose
    public String medicalinsuranceName;
    @SerializedName("medicalinsurance_number")
    @Expose
    public String medicalinsuranceNumber;
    @SerializedName("blood_type")
    @Expose
    public String bloodType;
    @SerializedName("height")
    @Expose
    public String height;
    @SerializedName("weight")
    @Expose
    public Integer weight;
    @SerializedName("allergies")
    @Expose
    public String allergies;
    @SerializedName("allergies_info")
    @Expose
    public String allergiesInfo;
    @SerializedName("district")
    @Expose
    public String district;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("treatment")
    @Expose
    public String treatment;
    @SerializedName("treatment_info")
    @Expose
    public String treatmentInfo;
    @SerializedName("profile_image")
    @Expose
    public String profileImage;
    @SerializedName("api_key")
    @Expose
    public String apiKey;
    @SerializedName("d_id")
    @Expose
    public Integer dId;

}