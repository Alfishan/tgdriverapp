package com.xeme.tgdriverapp.models;

/**
 * Created by root on 26/9/16.
 */
public class NetworkStateChanged
{
    public boolean mIsInternetConnected;
    public NetworkStateChanged(boolean isInternetConnected)
    {
        this.mIsInternetConnected = isInternetConnected;
    }
    public boolean isInternetConnected()
    {
        return this.mIsInternetConnected;
    }
}