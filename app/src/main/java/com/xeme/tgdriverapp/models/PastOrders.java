package com.xeme.tgdriverapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;



public class PastOrders {

    @SerializedName("error")
    @Expose
    public Boolean error;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("past_orders")
    @Expose
    public List<PastOrder> pastOrders = new ArrayList<PastOrder>();
}
