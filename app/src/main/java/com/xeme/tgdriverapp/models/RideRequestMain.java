package com.xeme.tgdriverapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 21/10/16.
 */

public class RideRequestMain {


    @SerializedName("error")
    @Expose
    public Boolean error;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("orders")
    @Expose
    public List<RideRequests> orders = new ArrayList<RideRequests>();

}

