package com.xeme.tgdriverapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.dsl.Table;

@Table
public class RideRequests {
    @SerializedName("pickup_area")
    @Expose
    public String pickupArea;
    @SerializedName("drop_area")
    @Expose
    public String dropArea;
    @SerializedName("land_mark")
    @Expose
    public String landMark;
    @SerializedName("distance")
    @Expose
    public String distance;
    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("order_number")
    @Expose
    public String orderNumber;
    @SerializedName("pickup_date")
    @Expose
    public String pickupDate;
    @SerializedName("pickup_time")
    @Expose
    public String pickupTime;
    @SerializedName("transfer_type")
    @Expose
    public Integer transferType;
    @SerializedName("service_type")
    @Expose
    public Integer serviceType;
    @SerializedName("payment_method")
    @Expose
    public String paymentMethod;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("o_id")
    @Expose
    private Long id;
    @SerializedName("source_latitude")
    @Expose
    public String sourceLatitude;
    @SerializedName("source_longitude")
    @Expose
    public String sourceLongitude;
    @SerializedName("des_latitude")
    @Expose
    public String desLatitude;
    @SerializedName("des_longitude")
    @Expose
    public String desLongitude;

    public RideRequests() {
    }

    public Long getId() {
        return id;
    }
}