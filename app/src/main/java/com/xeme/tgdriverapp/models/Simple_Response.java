package com.xeme.tgdriverapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 28/6/16.
 */
public class Simple_Response {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("error")
    @Expose
    public Boolean error;

    @SerializedName("img")
    @Expose
    public String img;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("last_modified")
    @Expose
    public String lastModified;

    @SerializedName("order_id")
    @Expose
    public Long o_id;

    @SerializedName("status")
    @Expose
    public Integer status;



}
