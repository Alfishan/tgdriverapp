package com.xeme.tgdriverapp.models;

/**
 * Created by root on 21/10/16.
 */

public class TestModel {

    int id;
    String Title;
    String SubTitle;
    long TimeStamp;

    public TestModel(int id, String title, String subTitle, long timeStamp) {
        this.id = id;
        Title = title;
        SubTitle = subTitle;
        TimeStamp = timeStamp;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return Title;
    }

    public String getSubTitle() {
        return SubTitle;
    }

    public long getTimeStamp() {
        return TimeStamp;
    }
}
