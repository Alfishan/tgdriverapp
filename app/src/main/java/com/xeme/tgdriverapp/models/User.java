package com.xeme.tgdriverapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 18/10/16.
 */

public class User {

    @SerializedName("error")
    @Expose
    public Boolean error;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("did")
    @Expose
    public Integer did;
    @SerializedName("nickname")
    @Expose
    public String nickname;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("pincode")
    @Expose
    public String pincode;
    @SerializedName("proof_type")
    @Expose
    public String proofType;
    @SerializedName("proof_number")
    @Expose
    public String proofNumber;
    @SerializedName("apikey")
    @Expose
    public String apikey;
    @SerializedName("current_balance")
    @Expose
    public Integer currentBalance;

}
