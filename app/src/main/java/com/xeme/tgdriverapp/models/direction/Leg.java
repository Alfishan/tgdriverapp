
package com.xeme.tgdriverapp.models.direction;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Leg {

    @SerializedName("distance")
    @Expose
    public Distance distance;
    @SerializedName("duration")
    @Expose
    public Duration duration;
    @SerializedName("end_address")
    @Expose
    public String endAddress;
    @SerializedName("end_location")
    @Expose
    public EndLocation endLocation;
    @SerializedName("start_address")
    @Expose
    public String startAddress;
    @SerializedName("start_location")
    @Expose
    public StartLocation startLocation;
    @SerializedName("steps")
    @Expose
    public List<Step> steps = new ArrayList<Step>();
    @SerializedName("traffic_speed_entry")
    @Expose
    public List<Object> trafficSpeedEntry = new ArrayList<Object>();
    @SerializedName("via_waypoint")
    @Expose
    public List<Object> viaWaypoint = new ArrayList<Object>();

}
