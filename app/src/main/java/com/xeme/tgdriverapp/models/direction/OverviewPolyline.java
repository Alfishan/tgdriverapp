
package com.xeme.tgdriverapp.models.direction;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class OverviewPolyline {

    @SerializedName("points")
    @Expose
    public String points;

}
