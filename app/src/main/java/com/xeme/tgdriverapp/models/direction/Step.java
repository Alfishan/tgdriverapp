
package com.xeme.tgdriverapp.models.direction;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Step {

    @SerializedName("distance")
    @Expose
    public Distance_ distance;
    @SerializedName("duration")
    @Expose
    public Duration_ duration;
    @SerializedName("end_location")
    @Expose
    public EndLocation_ endLocation;
    @SerializedName("html_instructions")
    @Expose
    public String htmlInstructions;
    @SerializedName("polyline")
    @Expose
    public Polyline polyline;
    @SerializedName("start_location")
    @Expose
    public StartLocation_ startLocation;
    @SerializedName("travel_mode")
    @Expose
    public String travelMode;
    @SerializedName("maneuver")
    @Expose
    public String maneuver;

}
