package com.xeme.tgdriverapp.services;


import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.xeme.tgdriverapp.R;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MySharedPreferences;

import static com.xeme.tgdriverapp.utils.MyHelper.Logwtf;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    final static int mNotifColor[] = {0, Color.WHITE, Color.RED, Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE, Color.MAGENTA};
    MySharedPreferences Mypref;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        Logwtf(MyConsta.TAG, "remoteMessage: " + remoteMessage);

        Mypref = MySharedPreferences.getInstance(this, MyConsta.My_Pref);
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

/*

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Logwtf(MyConsta.TAG, "Notif: " + remoteMessage.getNotification().toString());
        Logwtf(MyConsta.TAG, "RAW: " + remoteMessage.toString());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Logwtf(MyConsta.TAG, "Message data payload: " + remoteMessage.getData());
            //sendNotification("Notification Received:"+remoteMessage.getFrom());
            sendNotification(remoteMessage);
        }
*/

        // Check if message contains a notification payload.
       /* if (remoteMessage.getNotification() != null) {
            Logwtf(MyConsta.TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage);
        }
*/

        if (remoteMessage.getData().size() > 0) {
            Logwtf(MyConsta.TAG, "Message data payload: " + remoteMessage.getData());
            //sendNotification("Notification Received:"+remoteMessage.getFrom());
            sendNotification(remoteMessage);
          /*  if (remoteMessage.getData().containsKey("Sync")) {
                if (remoteMessage.getData().get("Sync").equals("1")) {
                    Intent SyncService=new Intent(this, MySyncService.class).setAction(MySyncService.SYNC_APPOINTMENT);
                    startService(SyncService);
                }
               else if (remoteMessage.getData().get("Sync").equals("2")) {
                    Intent SyncService=new Intent(this, MySyncService.class).setAction(MySyncService.SYNC_VITAL);
                    startService(SyncService);
                }
                else  if (remoteMessage.getData().get("Sync").equals("3")) {
                    Intent SyncService=new Intent(this, MySyncService.class).setAction(MySyncService.SYNC_RECOMMENDATION);
                    startService(SyncService);
                }
                else if (remoteMessage.getData().get("Sync").equals("4")) {
                    Intent SyncService=new Intent(this, MySyncService.class).setAction(MySyncService.SYNC_MEDICATION);
                    startService(SyncService);
                }
                else if (remoteMessage.getData().get("Sync").equals("5")) {
                    Intent SyncService=new Intent(this, MySyncService.class).setAction(MySyncService.SYNC_CHECKIN);
                    startService(SyncService);
                }
                else if (remoteMessage.getData().get("Sync").equals("6")) {
                    Intent SyncService=new Intent(this, MySyncService.class).setAction(MySyncService.SYNC_SYNCALLTYPE);
                    startService(SyncService);
                }
                else if (remoteMessage.getData().get("Sync").equals("7")) {
                    Intent SyncService=new Intent(this, MySyncService.class).setAction(MySyncService.SYNC_CHAT);
                    startService(SyncService);
                }*/
            }


        }




    /**
     * Create and show a simple notification containing the received FCM message.
     * <p>
     * //* @param messageBody FCM message body received.
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void sendNotification(RemoteMessage message) {


      /*  Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Uri NotifSound;
        if (Mypref.getBoolean(MyConsta.Settings_NotificationIsSet, false)) {
            NotifSound = Uri.parse(Mypref.getString(MyConsta.Settings_NotificationURI, defaultSoundUri.toString()));
        } else {
            NotifSound = defaultSoundUri;
        }
*/
        Intent intent = new Intent();
        if (message.getData().containsKey("click_action")) {
            intent.setAction(message.getData().get("click_action"));
        }
        else {
            intent.setAction("OPEN_ACTIVITY_786");
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(message.getData().get("title"))
                .setContentText(message.getData().get("subtitle"))
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);
/*

        switch (Mypref.getInt(MyConsta.Settings_NotificationVibration, MyConsta.Settings_NotificationVibration_Default)) {
            case MyConsta.Settings_NotificationVibration_Off:
                break;
            case MyConsta.Settings_NotificationVibration_Default:
                notificationBuilder.setVibrate(MyConsta.Virbation_Default);
                break;
            case MyConsta.Settings_NotificationVibration_Short:
                notificationBuilder.setVibrate(MyConsta.Virbation_Short);
                break;
            case MyConsta.Settings_NotificationVibration_Long:
                notificationBuilder.setVibrate(MyConsta.Virbation_Long);
                break;
        }

        int NotifColor = Mypref.getInt(MyConsta.Settings_NotificationLight, MyConsta.Settings_NotificationLight_None);
        if (NotifColor != 0) {
            notificationBuilder.setLights(mNotifColor[NotifColor], 1, 1);
        }
*/

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

}