package com.xeme.tgdriverapp.services;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.location.GnssStatus;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.xeme.tgdriverapp.MyApp;
import com.xeme.tgdriverapp.models.DialogResult;
import com.xeme.tgdriverapp.models.DoAction;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MyHelper;
import com.xeme.tgdriverapp.utils.MySharedPreferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;
import java.util.TimeZone;

import javax.inject.Inject;

import retrofit2.Retrofit;

import static com.xeme.tgdriverapp.utils.MyConsta.DoAction_GPS_Enabled;
import static com.xeme.tgdriverapp.utils.MyConsta.DoAction_GPS_Stopped;
import static com.xeme.tgdriverapp.utils.MyConsta.DoAction_GoogleApiDisconnected;
import static com.xeme.tgdriverapp.utils.MyHelper.Logwtf;


public class MyLocationService extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {


    public static GoogleApiClient mGoogleApiClient;
    public static LocationRequest mLocationRequest;
    public static Location mLastLocation;
    public static Location mCurrentLocation;
    public static boolean isConnected = false;
     Context mContext;
    Calendar mCalendar;
    int did;
    DatabaseReference databaseReference;
    NotificationManager notificationManager;
    String mTtff;
    public static  boolean GeoCoderAvailable=false;

    /**
     * Android N (7.0) and above status and listeners
     */
    //private GnssStatus mGnssStatus;
    private GnssStatus.Callback mGnssStatusListener;


    /**
     * Android M (6.0.1) and below status and listener
     */
    private GpsStatus mLegacyStatus;

    private GpsStatus.Listener mLegacyStatusListener;
    private GnssStatus mGnssStatus;


    public MyLocationService() {
    }
    @Inject
    MySharedPreferences mMyPref;
    @Inject
    Retrofit retrofit;
    @Inject
    EventBus mEventBus;

    LocationManager mLocationManager;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }




    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.wtf(MyConsta.TAG, "Service onStartCommand");
        mContext = MyLocationService.this;
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        createLocationRequest();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //CreateNotification();
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        addStatusListener();
        did = mMyPref.getInt(MyConsta.KEY_MYPID, 0);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mCalendar = Calendar.getInstance();
        mCalendar.setTimeZone(TimeZone.getTimeZone("IST"));
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
        removeStatusListener();

    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((MyApp) getApplication()).getNetComponent().inject(this);
        mEventBus.register(this);


    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        databaseReference.child("driver").child("location").child(String.valueOf(did)).setValue(location);

    }

    private void startLocationUpdates() {


        try {

            if (mGoogleApiClient != null) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);

                if (mGoogleApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(
                            mGoogleApiClient, mLocationRequest, this);

                }
            }
            else {
                Log.wtf(MyConsta.TAG, "********GoogLeApi Null******* :" );
            }


        } catch (SecurityException s) {

            Log.wtf(MyConsta.TAG, "********Error******* :" + s);
        }
    }



    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {



        try {
            isConnected = true;
            startLocationUpdates();

            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                // Determine whether a Geocoder is available.
                GeoCoderAvailable = Geocoder.isPresent();

            }




        } catch (SecurityException x) {
            Log.wtf(MyConsta.TAG, "********Error******* :" + x);
        }


    }

    @Override
    public void onConnectionSuspended(int i) {
        isConnected = false;
        EventBus.getDefault().post(new DoAction(DoAction_GoogleApiDisconnected));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(MyConsta.UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(MyConsta.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    long getTime() {

        if (mCalendar != null) {

            return mCalendar.getTimeInMillis();
        } else {
            mCalendar = Calendar.getInstance();
            return mCalendar.getTimeInMillis();

        }

    }


   /* public double getLat() {

        return mCurrentLocation.getLatitude();
    }

    public double getLng() {

        return mCurrentLocation.getLongitude();
    }*/

   /* void CreateNotification() {
       // Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.systemlogo)
                .setContentTitle("FSG GRIP")
                .setContentText("Aplicación se está ejecutando")
                .setAutoCancel(false).setPriority(1)
                .setOngoing(true); //.setSound(defaultSoundUri)

        Notification myNotif = notificationBuilder.build();
        myNotif.flags = Notification.FLAG_ONGOING_EVENT;
        notificationManager.notify(NotifID, myNotif);
        Log.wtf(MyConsta.TAG, "Notif Created" + NotifID);
    }*/

    @Subscribe
    public void DialogResult(String s){

    }

    private void addLegacyStatusListener() {

        try{


        mLegacyStatusListener = event -> {
            mLegacyStatus = mLocationManager.getGpsStatus(mLegacyStatus);

            switch (event) {
                case GpsStatus.GPS_EVENT_STARTED:

                    EventBus.getDefault().post(new DialogResult(10,true));
                    EventBus.getDefault().post(new DoAction(DoAction_GPS_Enabled));
                    break;
                case GpsStatus.GPS_EVENT_STOPPED:
                    EventBus.getDefault().post(new DialogResult(10,false));
                    EventBus.getDefault().post(new DoAction(DoAction_GPS_Stopped));
                    break;
                case GpsStatus.GPS_EVENT_FIRST_FIX:
                    int ttff = mLegacyStatus.getTimeToFirstFix();
                    if (ttff == 0) {
                        mTtff = "";
                    } else {
                        ttff = (ttff + 500) / 1000;
                        mTtff = Integer.toString(ttff) + " sec";
                    }
                    break;

            }

        };
        mLocationManager.addGpsStatusListener(mLegacyStatusListener);
        }
        catch (SecurityException e){
            Logwtf("addLegacyStatusListener SecurityException","Error"+e);
        }
    }


    @RequiresApi(Build.VERSION_CODES.N)
    private void addGnssStatusListener() {
      try {


          mGnssStatusListener = new GnssStatus.Callback() {
              @Override
              public void onStarted() {

              }

              @Override
              public void onStopped() {

              }

              @Override
              public void onFirstFix(int ttffMillis) {
                  if (ttffMillis == 0) {
                      mTtff = "";
                  } else {
                      ttffMillis = (ttffMillis + 500) / 1000;
                      mTtff = Integer.toString(ttffMillis) + " sec";
                  }

              }

              @Override
              public void onSatelliteStatusChanged(GnssStatus status) {
                  mGnssStatus = status;

              }
          };
          mLocationManager.registerGnssStatusCallback(mGnssStatusListener);
      }
      catch (SecurityException e){
          Logwtf("addGnssStatusListener SecurityException","Error"+e);
      }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private void removeGnssStatusListener() {
        mLocationManager.unregisterGnssStatusCallback(mGnssStatusListener);
    }
    private void removeLegacyStatusListener() {
        if (mLocationManager != null && mLegacyStatusListener != null) {
            mLocationManager.removeGpsStatusListener(mLegacyStatusListener);
        }
    }


    private void removeStatusListener() {
      //  SharedPreferences settings = Application.getPrefs();
        boolean useGnssApis = mMyPref.getBoolean("use_gnss_apis", true);

            if (MyHelper.isGnssStatusListenerSupported() && useGnssApis) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    removeGnssStatusListener();
                }
            } else {
            removeLegacyStatusListener();
        }
    }


    private void addStatusListener() {

        boolean useGnssApis = mMyPref.getBoolean("use_gnss_apis", true);

        if (MyHelper.isGnssStatusListenerSupported() && useGnssApis) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                addGnssStatusListener();
            }
        } else {
            addLegacyStatusListener();
        }
    }
}
