package com.xeme.tgdriverapp.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.google.firebase.iid.FirebaseInstanceId;
import com.orm.SugarRecord;
import com.xeme.tgdriverapp.MyApp;
import com.xeme.tgdriverapp.models.DoAction;
import com.xeme.tgdriverapp.models.NetworkStateChanged;
import com.xeme.tgdriverapp.models.PastOrders;
import com.xeme.tgdriverapp.models.RideRequestMain;
import com.xeme.tgdriverapp.models.RideRequests;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MySharedPreferences;
import com.xeme.tgdriverapp.utils.SlimApi;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.xeme.tgdriverapp.MyApp.UpdateNotifToken;
import static com.xeme.tgdriverapp.utils.MyConsta.DoAction_SyncHOme;
import static com.xeme.tgdriverapp.utils.MyConsta.DoAction_SyncRideHistory;
import static com.xeme.tgdriverapp.utils.MyConsta.KEY_IsLoggedIn;
import static com.xeme.tgdriverapp.utils.MyHelper.Logwtf;

public class MySyncService extends Service {


    public static final int SYNC_NewRideReqs = 0;
    public static final String SYNC_NewRideReq = "SYNC_NewRideReqs";

    public static final int SYNC_PastOrders = 2;
    public static final String SYNC_PastOrder = "SYNC_PastOrders";
    public static final int SYNC_Alls = 11;
    public static final String SYNC_All = "SYNC_Alls";


    @Inject
    MySharedPreferences mMyPref;
    @Inject
    Retrofit retrofit;
    @Inject
    EventBus mEventBus;


    public MySyncService() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
    }

    @Override
    public IBinder onBind(Intent intent) {

        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            if (intent.getAction().equals(SYNC_NewRideReq)) {
                SyncDB(SYNC_NewRideReqs);
            }
            else if (intent.getAction().equals(SYNC_PastOrder)) {
                SyncDB(SYNC_PastOrders);
            }

            else if (intent.getAction().equals(SYNC_All)) {
                SyncDB(SYNC_Alls);
            }
        }

        return Service.START_STICKY;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        ((MyApp) getApplication()).getNetComponent().inject(this);
        mEventBus.register(this);
    }


    void SyncDB(int SyncWhat) {

        if (mMyPref.getBoolean(MyConsta.KEY_IsInternet, true)) {

            switch (SyncWhat) {

                case SYNC_Alls:
                    SyncNewRideReqs();
                    SyncPastOrders();
                    break;

                case SYNC_NewRideReqs:
                    SyncNewRideReqs();
                    break;

                case SYNC_PastOrders:
                    SyncPastOrders();
                    break;

            }
        } else {
            Logwtf("SyncDB", "No internet Connection.Worthless To sync");
        }


    }


    @Subscribe
    public void onEventMainThread(NetworkStateChanged event) {
        if (event.isInternetConnected()) {
            Logwtf("onEventMainThread", "Got Internet?" + event.isInternetConnected());
            if (FirebaseInstanceId.getInstance().getToken() != null) {

                if (mMyPref.getBoolean(KEY_IsLoggedIn, false)) {

                    if (!mMyPref.getString(MyConsta.KEY_FBTOKEN, "").equals(FirebaseInstanceId.getInstance().getToken())) {
                        UpdateNotifToken(retrofit, mMyPref.getInt(MyConsta.KEY_MYPID, 0), mMyPref.getString(MyConsta.KEY_UUID, ""));
                    }
                }
            }

            //Toast.makeText(this, "No Internet connection!", Toast.LENGTH_SHORT).show();
        } else {
            Logwtf("onEventMainThread", "Got Internet?" + event.isInternetConnected());
            //Toast.makeText(this, " Internet connection!", Toast.LENGTH_SHORT).show();
        }
    }


    private void SyncNewRideReqs() {

        int DID = mMyPref.getInt(MyConsta.KEY_MYPID, 0);

        // String KEY = mMyPref.getString(MyConsta.KEY_UUID,"");

        SlimApi mSlimApi = retrofit.create(SlimApi.class);
        Observable<Response<RideRequestMain>> mNewRideReqs = mSlimApi.GetRideRequestsRX(DID);
        mNewRideReqs.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<RideRequestMain>>() {
                    @Override
                    public void onCompleted() {

                        EventBus.getDefault().post(new DoAction(DoAction_SyncHOme));
                    }

                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            Logwtf(getClass().getSimpleName() + "SyncNewRideReqs ", "onError:" + body.toString());
                        }
                        if (e instanceof IOException) {
                            IOException ioException = (IOException) e;

                            Logwtf(getClass().getSimpleName() + "SyncNewRideReqs ", "onError:" + e.getMessage());
                            e.getMessage();
                            // A network or conversion error happened
                        }
                        PutSyncTime(SYNC_NewRideReq, true);


                    }

                    @Override
                    public void onNext(Response<RideRequestMain> rideRequestMain) {

                       if (rideRequestMain.isSuccessful()) {
                           SugarRecord.deleteAll(RideRequests.class);
                        if (rideRequestMain.body().orders != null) {
                            if (rideRequestMain.body().orders.size() > 0) {
                                SugarRecord.saveInTx(rideRequestMain.body().orders);
                                PutSyncTime(SYNC_NewRideReq, false);
                                Logwtf("SyncNewRideReqs", "Synced Data:" + (rideRequestMain.body().orders.size()));

                            }

                        }
                       }
                        else if (rideRequestMain.code() == 201) {

                            Logwtf("SyncNewRideReqs", rideRequestMain.headers().toString() + "\n" + rideRequestMain.raw() + "\n Response msg" + rideRequestMain.body().message);


                        } else {
                            Logwtf("SyncNewRideReqs", rideRequestMain.headers().toString() + "\n" + rideRequestMain.raw() + "\n Response msg" + rideRequestMain.raw().toString());
                        }

                    }
                });

    }

    private void SyncPastOrders() {

        Logwtf(getClass().getSimpleName() + "SyncPastOrders ", "In Top");

        int DID = mMyPref.getInt(MyConsta.KEY_MYPID, 0);
        String KEY = mMyPref.getString(MyConsta.KEY_UUID, "");
        SlimApi mSlimApi = retrofit.create(SlimApi.class);
        Observable<Response<PastOrders>> mResponseObservable = mSlimApi.PostPastOrdersRX(KEY, DID);
        mResponseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<PastOrders>>() {
                    @Override
                    public void onCompleted() {
                        Logwtf(getClass().getSimpleName() + "SyncPastOrders ", "onCompleted");
                        EventBus.getDefault().post(new DoAction(DoAction_SyncRideHistory));
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logwtf(getClass().getSimpleName() + "SyncPastOrders ", "onError"+e.getCause());
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            Logwtf(getClass().getSimpleName() + "SyncPastOrders ", "onError:" + body.toString());
                        }
                        if (e instanceof IOException) {
                            IOException ioException = (IOException) e;

                            Logwtf(getClass().getSimpleName() + "SyncPastOrders ", "onError:" + e.getMessage());
                            e.getMessage();
                            // A network or conversion error happened
                        }
                        PutSyncTime(SYNC_PastOrder, true);


                    }

                    @Override
                    public void onNext(Response<PastOrders> pastOrders) {
                        Logwtf(getClass().getSimpleName() + "SyncPastOrders ", "onNext");
                        if (pastOrders.isSuccessful()) {

                            if (!pastOrders.body().error) {

                                if (pastOrders.body().pastOrders != null) {
                                    if (pastOrders.body().pastOrders.size() > 0) {
                                        SugarRecord.saveInTx(pastOrders.body().pastOrders);
                                        PutSyncTime(SYNC_PastOrder, false);
                                        Logwtf("SyncPastOrders", "Synced Data:" + (pastOrders.body().pastOrders.size()));

                                    }
                                }
                                else {
                                    Logwtf("SyncPastOrders", pastOrders.headers().toString() + "\n" + pastOrders.raw() + "\n Response Past Order Null");
                                }
                            }
                            else {
                                Logwtf("SyncPastOrders", pastOrders.headers().toString() + "\n" + pastOrders.raw() + "\n Response msg" + pastOrders.body().message);
                            }

                        }else if (pastOrders.code() == 201) {

                            Logwtf("SyncPastOrders", pastOrders.headers().toString() + "\n" + pastOrders.raw() + "\n Response msg" + pastOrders.body().message);


                        } else {
                            Logwtf("SyncPastOrders", pastOrders.headers().toString() + "\n" + pastOrders.raw() + "\n Response msg" + pastOrders.raw().toString());
                        }
                    }
                });

    }

    long getTimeNow() {
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("GMT"));
        return c.getTimeInMillis();
    }


    void PutSyncTime(String Value1, boolean Value2) {
        mMyPref.putBoolean(Value1, Value2);
        mMyPref.putLong(Value1, getTimeNow());
    }


}


