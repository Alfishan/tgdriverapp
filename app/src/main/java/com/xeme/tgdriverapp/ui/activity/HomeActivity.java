package com.xeme.tgdriverapp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.orm.SugarRecord;
import com.xeme.tgdriverapp.MyApp;
import com.xeme.tgdriverapp.R;
import com.xeme.tgdriverapp.adapters.Adap_Home;
import com.xeme.tgdriverapp.models.DialogResult;
import com.xeme.tgdriverapp.models.DoAction;
import com.xeme.tgdriverapp.models.RideRequests;
import com.xeme.tgdriverapp.models.Simple_Response;
import com.xeme.tgdriverapp.ui.dialog.MyProgressDialog;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MyHelper;
import com.xeme.tgdriverapp.utils.MySharedPreferences;
import com.xeme.tgdriverapp.utils.SlimApi;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.xeme.tgdriverapp.utils.MyConsta.DoAction_SyncHOme;
import static com.xeme.tgdriverapp.utils.MyConsta.KEY_RID;
import static com.xeme.tgdriverapp.utils.MyHelper.Logwtf;

public class HomeActivity extends AppCompatActivity implements Adap_Home.AdapHomeHelper, NavigationView.OnNavigationItemSelectedListener {


    @Inject
    MySharedPreferences mMyPref;
    @Inject
    Retrofit retrofit;
    @Inject
    EventBus mEventBus;
    @BindView(R.id.mt_sc_on_work)
    SwitchCompat mtScOnWork;
    @BindView(R.id.toolbar_main)
    Toolbar toolbarMain;
    @BindView(R.id.ha_rv_main)
    RecyclerView haRvMain;
    @BindView(R.id.content_home)
    RelativeLayout contentHome;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.ha_clayout)
    CoordinatorLayout haClayout;
    private Adap_Home mAdapHome;
    private MyProgressDialog mMyProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_main);
        ButterKnife.bind(this);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        mEventBus.register(this);
        setSupportActionBar(toolbarMain);
        getSupportActionBar().setTitle("New Ride Requests");
        InitViews();

    }

    private void InitViews() {
        mMyProgressDialog = new MyProgressDialog(this);
        mtScOnWork.setChecked(mMyPref.getBoolean("OnWork", false));
        mtScOnWork.setOnCheckedChangeListener((compoundButton, b) -> {

        });

        mtScOnWork.setOnClickListener(view -> {


            boolean Status = mtScOnWork.isChecked();
            if (mMyPref.getBoolean(MyConsta.KEY_IsInternet, true)) {

                DriverStatus(mtScOnWork.isChecked());

            } else {
                mtScOnWork.setChecked(!Status);
                MyHelper.ShowSnackBar(haClayout, "Please enable internet.", MyHelper.Snack_Red);
            }


        });
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbarMain, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //noinspection deprecation
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        navView.setNavigationItemSelectedListener(this);
        InitNavVies();
        initRv();
        // GetRideRequests();
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.hda_history) {
            // Handle the camera action
            Intent i = new Intent(HomeActivity.this, RideHistoryActivity.class);
            startActivity(i);
        } /*else if (id == R.id.emergency_contact) {

            Intent ECI = new Intent(HomeActivity.this, ChatMasterActivity.class).putExtra("Goto", 1);
            startActivity(ECI);

        } else if (id == R.id.datasync) {
            EventBus.getDefault().post(new Simple_Response(true));
        }*/

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    void InitNavVies() {


        View headerView = navView.inflateHeaderView(R.layout.nav_header_home_drawer);
        AppCompatTextView navTitle = (AppCompatTextView) headerView.findViewById(R.id.nhhd_title);
        AppCompatImageView navImage = (AppCompatImageView) headerView.findViewById(R.id.nhhd_image);
        navTitle.setTypeface(MyApp.Typeface_Light);
        String mFullName = "Tommy Singh";
        if (mFullName.equals(""))
            mFullName = "HeaLink";
        navTitle.setText(mFullName);


    /*    Picasso.with(HomeActivity.this).load(MyApp.InfoGetProfilePic()).resize(navView.getWidth(), MyHelper.dpToPx(160)).centerCrop().placeholder(R.drawable.progress_animation).into(navImage, new Callback() {
            @Override
            public void onSuccess() {

                Palette.from(((BitmapDrawable) navImage.getDrawable()).getBitmap()).maximumColorCount(16).generate(palette -> {
                    Palette.Swatch swatch = palette.getDarkMutedSwatch();


                    if (swatch != null) {
                        // Gets the RGB packed int -> same as palette.getVibrantColor(defaultColor);
                        int rgbColor = swatch.getRgb();
                        // Gets the HSL values
                        // Hue between 0 and 360
                        // Saturation between 0 and 1
                        // Lightness between 0 and 1
                        float[] hslValues = swatch.getHsl();
                        // Gets the number of pixels represented by this swatch
                        int pixelCount = swatch.getPopulation();
                        // Gets an appropriate title text color
                        int titleTextColor = swatch.getTitleTextColor();
                        // Gets an appropriate body text color
                        int bodyTextColor = swatch.getBodyTextColor();


                        int colorFrom = navView.getDrawingCacheBackgroundColor();
                        int colorTo = rgbColor;
                        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
                        colorAnimation.setDuration(2000); // milliseconds
                        colorAnimation.addUpdateListener(animator -> navView.setBackgroundColor((int) animator.getAnimatedValue()));
                        colorAnimation.start();


                        // navView.setBackgroundColor(rgbColor);
                        navTitle.setTextColor(bodyTextColor);
                    } else {
                        Palette.Swatch swatch2 = palette.getLightVibrantSwatch();

                        if (swatch2 != null) {
                            int rgbColor = swatch2.getRgb();
                            // Gets the HSL values
                            // Hue between 0 and 360
                            // Saturation between 0 and 1
                            // Lightness between 0 and 1
                            float[] hslValues = swatch2.getHsl();
                            // Gets the number of pixels represented by this swatch
                            int pixelCount = swatch2.getPopulation();
                            // Gets an appropriate title text color
                            int titleTextColor = swatch2.getTitleTextColor();
                            // Gets an appropriate body text color
                            int bodyTextColor = swatch2.getBodyTextColor();

                            navView.setBackgroundColor(rgbColor);
                            navTitle.setTextColor(titleTextColor);
                        }
                    }


                });
            }

            @Override
            public void onError() {
                Picasso.with(HomeActivity.this).load(MyConsta.Default_Profile).resize(navView.getWidth(), MyHelper.dpToPx(160)).centerCrop().placeholder(R.drawable.progress_animation).into(navImage, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });
            }
        });
  */
    }

    private void initRv() {
        mMyProgressDialog.ShowDialog("Please Wait...");
        List<RideRequests> mRequestsList = SugarRecord.listAll(RideRequests.class);
        mAdapHome = new Adap_Home(mRequestsList, this, this, "No Record For now");
        haRvMain.setLayoutManager(new LinearLayoutManager(this));
        haRvMain.setAdapter(mAdapHome);
        RecyclerView.ItemAnimator mItemAnimator = new DefaultItemAnimator();
        mItemAnimator.setAddDuration(500);
        mItemAnimator.setRemoveDuration(500);
        haRvMain.setItemAnimator(mItemAnimator);
        mMyProgressDialog.hideDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
    }

    @Subscribe
    public void DialogResult(DialogResult mResult) {


      /*  if (mResult.getForWho() == 10) {

           *//* if (mResult.isResult()) {
                MyHelper.ShowSnackBar(haClayout, "Gps Enabled", MyHelper.Snack_Green);

            } else {
                MyHelper.ShowSnackBar(haClayout, "GPS_EVENT_STOPPED", MyHelper.Snack_Red);

            }*//*
        }*/
    }


  /*  void GetRideRequests() {
        int Did = mMyPref.getInt(MyConsta.KEY_MYPID, 0);
        if (mMyPref.getBoolean(MyConsta.KEY_IsInternet, true)) {
            mMyProgressDialog.ShowDialog(" Please Wait...");
            Call<RideRequestMain> UpdateMedCall = retrofit.create(SlimApi.class).GetRideRequests(Did);
            UpdateMedCall.enqueue(new Callback<RideRequestMain>() {
                @Override
                public void onResponse(Call<RideRequestMain> call, Response<RideRequestMain> response) {

                    if (response != null) {
                        if (response.code() == MyConsta.RESPONSE_OK) {

                            if (!response.body().error) {

                                if (response.body().orders != null) {
                                   // initRv(response.body().orders);
                                }

                            } else {
                                Toast.makeText(HomeActivity.this, response.body().message, Toast.LENGTH_LONG).show();
                                // Logwtf("UpdateMedication", "Status Update Failed for ID:" + masterCommun.id);
                            }
                            mMyProgressDialog.hideDialog();

                        } else if (response.code() == MyConsta.RESPONSE_MISSING) {
                            if (response.body().error) {
                                Logwtf("DoAuth()", "Missing Params:" + response.body().message);

                            }
                            mMyProgressDialog.hideDialog();
                        } else if (response.code() == MyConsta.RESPONSE_SERVER_ERROR) {
                            mMyProgressDialog.hideDialog();
                            // Toast.makeText(LoginActivity.this, "Server Error. Please Try After Some Time", Toast.LENGTH_SHORT).show();
                        } else {
                            mMyProgressDialog.hideDialog();
                            //Toast.makeText(LoginActivity.this, "UnKnown Error!!!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mMyProgressDialog.hideDialog();
                    }

                }

                @Override
                public void onFailure(Call<RideRequestMain> call, Throwable t) {
                    mMyProgressDialog.hideDialog();
                    Toast.makeText(HomeActivity.this, "Error!!! Please Try after sometime", Toast.LENGTH_SHORT).show();
                    Logwtf("UpdateAppointment()", "onFailure :" + t.getCause());
                }
            });
        } else {

            MyHelper.ShowSnackBar(this.getWindow().getDecorView(), "No Internet Available. Please Check internet Connection.", MyHelper.Snack_Red);
            //Toast.makeText(mHomeActivity, "No Internet Available. Please Check internet Connection.", Toast.LENGTH_LONG).show();

        }


    }*/

    @Override
    public void AcceptReq(RideRequests SelectedData, int pos) {


        if (mMyPref.getBoolean(MyConsta.KEY_IsInternet, true)) {

            AcceptRideRequestRX(SelectedData);

        } else {
            MyHelper.ShowSnackBar(haClayout, "Please enable internet to accept this request.", MyHelper.Snack_Red);
        }

    }


    void AcceptRideRequestRX(RideRequests SelectedData) {
        int DID = mMyPref.getInt(MyConsta.KEY_MYPID, 0);
        mMyProgressDialog.ShowDialog("Accepting Ride. please wait");
        SlimApi mSlimApi = retrofit.create(SlimApi.class);
        Observable<Response<Simple_Response>> mAcceptRideReq = mSlimApi.PostAcceptOrderRX(DID, SelectedData.getId(), MyConsta.Ride_Status_Booking);
        mAcceptRideReq.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<Simple_Response>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        MyHelper.ShowSnackBar(haClayout, "Something went wrong. please try again later.", MyHelper.Snack_Red);

                    }

                    @Override
                    public void onNext(Response<Simple_Response> msimple_response) {

                        mMyProgressDialog.hideDialog();
                        if (msimple_response.isSuccessful()) {
                            if (!msimple_response.body().error) {
                                MyHelper.ShowSnackBar(haClayout, "Order Accepted.Please Reach to the customer.", MyHelper.Snack_Green);
                                Intent i = new Intent(HomeActivity.this, RideRequestActivity.class);
                                i.putExtra(KEY_RID, SelectedData.getId());
                                startActivity(i);

                            } else {
                                MyHelper.ShowSnackBar(haClayout, msimple_response.body().message, MyHelper.Snack_Red);
                            }

                        } else if (msimple_response.code() == 201) {

                            Logwtf("DriverStatus", msimple_response.headers().toString() + "\n" + msimple_response.raw() + "\n Response msg" + msimple_response.body().message);


                        } else {
                            Logwtf("DriverStatus", msimple_response.headers().toString() + "\n" + msimple_response.raw() + "\n Response msg" + msimple_response.raw().toString());
                        }


                    }
                });
    }

    @Subscribe
    public void DoActionHome(DoAction mDoAction) {
        if (mDoAction.DoWhat == DoAction_SyncHOme) {
            Logwtf("DoActionHome", "Sync Baby Sync");
            initRv();
        }
    }

    void DriverStatus(boolean Status) {

        int IsActive = Status ? 1 : 0;


        int DID = mMyPref.getInt(MyConsta.KEY_MYPID, 0);
        SlimApi mSlimApi = retrofit.create(SlimApi.class);
        Observable<Response<Simple_Response>> mResponseObservable = mSlimApi.PostDriverStatus(IsActive, DID);
        mResponseObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<Simple_Response>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Logwtf(getClass().getSimpleName() + "DriverStatus ", "onError" + e.getCause());
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            Logwtf(getClass().getSimpleName() + "DriverStatus ", "onError:" + body.toString());
                        }
                        if (e instanceof IOException) {
                            IOException ioException = (IOException) e;
                            Logwtf(getClass().getSimpleName() + "DriverStatus ", "onError:" + e.getMessage());
                            e.getMessage();
                            // A network or conversion error happened
                        }
                        mtScOnWork.setChecked(!Status);

                    }

                    @Override
                    public void onNext(Response<Simple_Response> response) {

                        if (response.isSuccessful()) {

                            if (!response.body().error) {


                                mtScOnWork.setChecked(Status);
                                mMyPref.putBoolean("OnWork", Status);
                                if (Status)
                                    MyHelper.ShowSnackBar(haClayout, "You Are Online now.", MyHelper.Snack_Green);
                                else
                                    MyHelper.ShowSnackBar(haClayout, "You Are Offline now.", MyHelper.Snack_Red);


                            } else {
                                mtScOnWork.setChecked(!Status);
                                Logwtf("DriverStatus", response.headers().toString() + "\n" + response.raw() + "\n Response msg" + response.body().message);
                            }

                        } else if (response.code() == 201) {

                            Logwtf("DriverStatus", response.headers().toString() + "\n" + response.raw() + "\n Response msg" + response.body().message);


                        } else {
                            Logwtf("DriverStatus", response.headers().toString() + "\n" + response.raw() + "\n Response msg" + response.raw().toString());
                        }
                    }
                });

    }

}



