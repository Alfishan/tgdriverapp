package com.xeme.tgdriverapp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.xeme.tgdriverapp.MyApp;
import com.xeme.tgdriverapp.R;
import com.xeme.tgdriverapp.models.User;
import com.xeme.tgdriverapp.services.MyLocationService;
import com.xeme.tgdriverapp.services.MySyncService;
import com.xeme.tgdriverapp.ui.dialog.MyProgressDialog;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MyHelper;
import com.xeme.tgdriverapp.utils.MySharedPreferences;
import com.xeme.tgdriverapp.utils.SlimApi;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.xeme.tgdriverapp.MyApp.UpdateNotifToken;
import static com.xeme.tgdriverapp.utils.MyHelper.Logwtf;


public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener {

    @BindView(R.id.la_aciv_logo)
    AppCompatImageView laAcivLogo;

    @Inject
    MySharedPreferences mMyPref;
    @Inject
    Retrofit retrofit;
    MyProgressDialog mProgressDialog;
    Validator mValidator;


    @Email()
    @BindView(R.id.la_acet_email)
    AppCompatEditText laAcetEmail;

    @Password(min = 6, scheme = Password.Scheme.ANY, message = "min 6 character")
    @BindView(R.id.la_acet_password)
    AppCompatEditText laAcetPassword;

    @BindView(R.id.la_acbtn_login)
    AppCompatButton laAcbtnLogin;
    @BindView(R.id.la_ll_main)
    LinearLayout laLlMain;
   /* @BindView(R.id.la_acbtn_forgot_password)
    AppCompatButton laAcbtnForgotPassword;*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        YoYo.with(Techniques.Flash).delay(300).duration(1000).playOn(laLlMain);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        initviews();

    }

    private void initviews() {

        mProgressDialog = new MyProgressDialog(LoginActivity.this);
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
        // laAcbtnForgotPassword.setText(Html.fromHtml("<u><b>" + getResources().getString(R.string.did_you_forget_your_login_information) + "</b></u>"));


        // [START initialize_auth]

    }

    @OnClick({R.id.la_acbtn_login})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.la_acbtn_login:
                mValidator.validate();
                break;
            /*case R.id.la_acbtn_forgot_password:
                break;*/
        }
    }

    @Override
    public void onValidationSucceeded() {

        if (mMyPref.getBoolean(MyConsta.KEY_IsInternet, true)) {
            DoAuth();
        } else {
            MyHelper.ShowSnackBar(this.getWindow().getDecorView(), MyHelper.Snack_msg_NoInternet, MyHelper.Snack_Red);
        }


    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                AnimateViewShake(view);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void AnimateViewShake(View view) {

        if (view != null) {
            YoYo.with(Techniques.Shake).duration(500).playOn(view);
        }


    }


    void DoAuth() {

        mProgressDialog.ShowDialog("Authenticating... Please wait");
        String Email = laAcetEmail.getText().toString().trim();
        String Password = laAcetPassword.getText().toString().trim();

        EnableDisableViews(1);

        // Observable<LoginResponse> ob=retrofit.create(SlimApi.class).GetLoginDetailswithRx(Email, Password);

        Call<User> AuthCall = retrofit.create(SlimApi.class).GetLoginDetails(Email, Password);
        AuthCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if (response != null) {
                    if (response.code() == MyConsta.RESPONSE_OK) {

                        if (!response.body().error) {



                            mMyPref.putBoolean(MyConsta.KEY_IsLoggedIn, true);
                            UpdateNotifToken(retrofit,response.body().did,response.body().apikey);
                            mMyPref.putLong(MyConsta.KEY_IsLoggedInTime, Calendar.getInstance().getTimeInMillis());
                            MyApp.SaveUserProfile(response.body());
                            mMyPref.putBoolean("Init", true);
                            GoToHome();

                        } else {
                            YoYo.with(Techniques.Shake).duration(500).playOn(laAcetEmail);
                            YoYo.with(Techniques.Shake).duration(500).playOn(laAcetPassword);
                            Toast.makeText(LoginActivity.this, response.body().message, Toast.LENGTH_SHORT).show();
                            EnableDisableViews(0);
                        }

                    } else if (response.code() == MyConsta.RESPONSE_MISSING) {
                        if (response.body().error) {
                            Logwtf("DoAuth()", "Missing Params:" + response.body().message);
                            EnableDisableViews(0);
                        }
                    } else if (response.code() == MyConsta.RESPONSE_SERVER_ERROR) {
                        EnableDisableViews(0);
                        Toast.makeText(LoginActivity.this, "Server Error. Please Try After Some Time", Toast.LENGTH_SHORT).show();
                    } else {
                        EnableDisableViews(0);
                        Toast.makeText(LoginActivity.this, "UnKnown Error!!!", Toast.LENGTH_SHORT).show();
                    }
                }

                mProgressDialog.hideDialog();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                EnableDisableViews(0);
                mProgressDialog.hideDialog();
                Toast.makeText(LoginActivity.this, "Error!!! Please Try after sometime", Toast.LENGTH_SHORT).show();
                Logwtf("DoAuth()", "onFailure :" + t.getCause());

            }
        });


        /*AuthCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });*/
    }



    void GoToHome(){

        startActivity(new Intent(this,HomeActivity.class));
        Intent LocationService = new Intent(this, MyLocationService.class);
        startService(LocationService);
        Intent SyncService = new Intent(this, MySyncService.class).setAction(MySyncService.SYNC_All);
        startService(SyncService);
        Logwtf(getClass().getSimpleName()+" Redirect()", "now Checking Time:All ok");
        finish();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();


    }

    void EnableDisableViews(int who) {
        switch (who) {
            case 0:

                laAcetEmail.setEnabled(true);
                laAcetEmail.setAlpha(1f);
                laAcetPassword.setEnabled(true);
                laAcetPassword.setAlpha(1f);
                laAcbtnLogin.setEnabled(true);
                laAcbtnLogin.setAlpha(1f);
                break;
            case 1:

                laAcetEmail.setEnabled(false);
                laAcetEmail.setAlpha(0.6f);
                laAcetPassword.setEnabled(false);
                laAcetPassword.setAlpha(0.6f);
                laAcbtnLogin.setEnabled(false);
                laAcbtnLogin.setAlpha(0.6f);
                break;
        }


    }
}
