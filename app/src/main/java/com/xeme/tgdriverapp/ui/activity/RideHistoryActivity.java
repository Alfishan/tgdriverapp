package com.xeme.tgdriverapp.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.orm.SugarRecord;
import com.xeme.tgdriverapp.MyApp;
import com.xeme.tgdriverapp.R;
import com.xeme.tgdriverapp.adapters.Adap_RideHistory;
import com.xeme.tgdriverapp.models.DoAction;
import com.xeme.tgdriverapp.models.PastOrder;
import com.xeme.tgdriverapp.ui.dialog.MyProgressDialog;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MySharedPreferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Retrofit;

import static com.xeme.tgdriverapp.utils.MyConsta.DoAction_SyncRideHistory;
import static com.xeme.tgdriverapp.utils.MyHelper.Logwtf;

public class RideHistoryActivity extends AppCompatActivity implements Adap_RideHistory.AdapHomeHelper {

    @BindView(R.id.rha_toolbar)
    Toolbar rhaToolbar;
    @BindView(R.id.rha_rv)
    RecyclerView rhaRv;
    @BindView(R.id.content_ride_history)
    LinearLayout contentRideHistory;
    private MyProgressDialog mMyProgressDialog;
    @Inject
    MySharedPreferences mMyPref;
    @Inject
    Retrofit retrofit;

    @Inject
    EventBus mEventBus;

    private Adap_RideHistory mAdapRideHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_history);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        ButterKnife.bind(this);

        mEventBus.register(this);
        setSupportActionBar(rhaToolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initviews();
    }

    void initviews(){
        mMyProgressDialog=new MyProgressDialog(this);
        initRv();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void DoActionRRA(DoAction mDoAction) {
        if (mDoAction.DoWhat == MyConsta.DoAction_Refresh_RideHistory) {
            Toast.makeText(this, "Updating RideHistory", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
    }



    private void initRv() {
        mMyProgressDialog.ShowDialog(" Please Wait...");
        List<PastOrder> mRequestsList= SugarRecord.listAll(PastOrder.class);
        mAdapRideHistory = new Adap_RideHistory(mRequestsList, this, this, "No Record For now");
        rhaRv.setLayoutManager(new LinearLayoutManager(this));
        rhaRv.setAdapter(mAdapRideHistory);
        RecyclerView.ItemAnimator mItemAnimator = new DefaultItemAnimator();
        mItemAnimator.setAddDuration(500);
        mItemAnimator.setRemoveDuration(500);
        rhaRv.setItemAnimator(mItemAnimator);
        mMyProgressDialog.hideDialog();
    }


    @Subscribe
    public void DoActionRHA(DoAction mDoAction) {

        if (mDoAction.DoWhat == DoAction_SyncRideHistory) {
            Logwtf("DoActionRHA", "Sync Baby Sync");
            initRv();
        }


    }

}
