package com.xeme.tgdriverapp.ui.activity;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import com.orm.SugarRecord;
import com.xeme.tgdriverapp.MyApp;
import com.xeme.tgdriverapp.R;
import com.xeme.tgdriverapp.models.DialogResult;
import com.xeme.tgdriverapp.models.DoAction;
import com.xeme.tgdriverapp.models.RideRequests;
import com.xeme.tgdriverapp.models.Simple_Response;
import com.xeme.tgdriverapp.ui.dialog.MyProgressDialog;
import com.xeme.tgdriverapp.ui.fragment.Frag_RR_StartStop;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MyHelper;
import com.xeme.tgdriverapp.utils.MySharedPreferences;
import com.xeme.tgdriverapp.utils.SlimApi;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.xeme.tgdriverapp.utils.MyHelper.Logwtf;

public class RideRequestActivity extends AppCompatActivity {


    @BindView(R.id.rra_fl_main)
    FrameLayout rraFlMain;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    @BindView(R.id.rra_toolbar)
    Toolbar rraToolbar;
    @BindView(R.id.rra_fab_myLocation)
    FloatingActionButton rraFabMyLocation;
    @BindView(R.id.rra_cl_main)
    CoordinatorLayout rraClMain;
    RideRequests mRequests;
    @Inject
    MySharedPreferences mMyPref;
    @Inject
    Retrofit retrofit;
    @Inject
    EventBus mEventBus;
    int DID;
    private MyProgressDialog mMyProgressDialog;
    public CoordinatorLayout getRraClMain() {
        return rraClMain;
    }

    public RideRequests getRequests() {
        return mRequests;
    }

    public void setRequests(RideRequests requests) {
        mRequests = requests;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_request);
        ButterKnife.bind(this);
        setSupportActionBar(rraToolbar);
        getSupportActionBar().setTitle("Ride Request");
        ((MyApp) getApplication()).getNetComponent().inject(this);
        mEventBus.register(this);
        if (getIntent() != null) {
            if (getIntent().hasExtra(MyConsta.KEY_RID)) {
                Long RID = getIntent().getLongExtra(MyConsta.KEY_RID, 0);
                Logwtf(getClass().getSimpleName(), "Rid" + RID);
                setRequests(SugarRecord.findById(RideRequests.class, RID));
                //                                                                                                                                                    Logwtf(getClass().getSimpleName() + " PickUp Area", mRequests.pickupArea);

            }
        }
         DID = mMyPref.getInt(MyConsta.KEY_MYPID,0);
        mMyProgressDialog = new MyProgressDialog(this);
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction
                .setCustomAnimations(R.anim.fadein, R.anim.fadeout, R.anim.fadein, R.anim.fadeout)
                .replace(R.id.rra_fl_main, new RideRequestActivityFragment()).commit();
        getSupportActionBar().setTitle("Pick up Customer");

    }

    @OnClick(R.id.rra_fab_myLocation)
    public void onClick() {


    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        MyHelper.showPopUpdialog(131, this, "Cancel Ride & exit?", "You want to cancel this Ride and exit. Are you sure?", "Yes", "No", false);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void DialogResult(DialogResult mResult) {

        if (mResult.getForWho() == 131) {

            if (mResult.isResult()) {
                StatusCancelRide();
            }
        }

    }



    void StatusCompleteRide(){

        mMyProgressDialog.ShowDialog("Accepting Ride. please wait");
        SlimApi mSlimApi = retrofit.create(SlimApi.class);
        Observable<Response<Simple_Response>> mAcceptRideReq = mSlimApi.PostAcceptOrderRX(DID,mRequests.getId(),MyConsta.Ride_Status_Complete);
        mAcceptRideReq.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<Simple_Response>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mMyProgressDialog.hideDialog();
                        MyHelper.ShowSnackBar(getWindow().getDecorView(),"Something went wrong. please try again.",MyHelper.Snack_Red);
                    }

                    @Override
                    public void onNext(Response<Simple_Response> msimple_response) {

                        mMyProgressDialog.hideDialog();
                        if (msimple_response.isSuccessful()) {
                            if (!msimple_response.body().error) {
                                MyHelper.ShowSnackBar(getWindow().getDecorView(),"Ride completed Successfully",MyHelper.Snack_Red);
                                 finish();
                            }
                            else {
                                MyHelper.ShowSnackBar(getWindow().getDecorView(),msimple_response.body().message,MyHelper.Snack_Red);
                            }

                        }
                        else if (msimple_response.code()==MyConsta.RESPONSE_SERVER_ERROR){

                        }


                    }
                });
    }

    void StatusStartRide(){

        mMyProgressDialog.ShowDialog("Accepting Ride. please wait");
        SlimApi mSlimApi = retrofit.create(SlimApi.class);
        Observable<Response<Simple_Response>> mAcceptRideReq = mSlimApi.PostAcceptOrderRX(DID,mRequests.getId(),MyConsta.Ride_Status_Start_trip);
        mAcceptRideReq.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<Simple_Response>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mMyProgressDialog.hideDialog();
                        MyHelper.ShowSnackBar(getWindow().getDecorView(),"Something went wrong. please try again.",MyHelper.Snack_Red);
                    }

                    @Override
                    public void onNext(Response<Simple_Response> msimple_response) {

                        mMyProgressDialog.hideDialog();
                        if (msimple_response.isSuccessful()) {
                            if (!msimple_response.body().error) {
                                GotoStartStopFrag();
                                MyHelper.ShowSnackBar(getWindow().getDecorView(),"Ok Now reach destination.",MyHelper.Snack_Red);
                            }
                            else {
                                MyHelper.ShowSnackBar(getWindow().getDecorView(),msimple_response.body().message,MyHelper.Snack_Red);
                            }

                        }
                        else if (msimple_response.code()==MyConsta.RESPONSE_SERVER_ERROR){

                        }


                    }
                });
    }

    void  StatusCancelRide(){
        mMyProgressDialog.ShowDialog("Canceling  Ride. please wait");
        SlimApi mSlimApi = retrofit.create(SlimApi.class);
        Observable<Response<Simple_Response>> mAcceptRideReq = mSlimApi.PostAcceptOrderRX(DID,mRequests.getId(),MyConsta.Ride_Status_Cancel);
        mAcceptRideReq.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<Simple_Response>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mMyProgressDialog.hideDialog();
                        MyHelper.ShowSnackBar(getWindow().getDecorView(),"Something went wrong. please try again.",MyHelper.Snack_Red);
                    }

                    @Override
                    public void onNext(Response<Simple_Response> msimple_response) {

                        mMyProgressDialog.hideDialog();
                        if (msimple_response.isSuccessful()) {
                            if (!msimple_response.body().error) {

                                MyHelper.ShowSnackBar(getWindow().getDecorView(),"Ride canceled.",MyHelper.Snack_Red);
                                finish();
                            }
                            else {
                                MyHelper.ShowSnackBar(getWindow().getDecorView(),msimple_response.body().message,MyHelper.Snack_Red);
                            }

                        }
                        else if (msimple_response.code()==MyConsta.RESPONSE_SERVER_ERROR){

                        }


                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void DoActionRRA(DoAction mDoAction) {
        if (mDoAction.DoWhat == 132) {

            if (mMyPref.getBoolean(MyConsta.KEY_IsInternet, true)) {
                StatusStartRide();

            } else {
                MyHelper.ShowSnackBar(rraClMain, "Please enable internet.", MyHelper.Snack_Red);
            }

        }


        if (mDoAction.DoWhat == 101) {

            if (mMyPref.getBoolean(MyConsta.KEY_IsInternet, true)) {
                StatusCompleteRide();
            } else {
                MyHelper.ShowSnackBar(rraClMain, "Please enable internet.", MyHelper.  Snack_Red);
            }



        }

    }


    void GotoStartStopFrag(){
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction
                .setCustomAnimations(R.anim.fadein, R.anim.fadeout, R.anim.fadein, R.anim.fadeout)
                .replace(R.id.rra_fl_main, new Frag_RR_StartStop()).commit();
        getSupportActionBar().setTitle("Reach Destination");
    }

}
