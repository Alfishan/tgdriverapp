package com.xeme.tgdriverapp.ui.activity;

import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.xeme.tgdriverapp.MyApp;
import com.xeme.tgdriverapp.R;
import com.xeme.tgdriverapp.models.DialogResult;
import com.xeme.tgdriverapp.models.DoAction;
import com.xeme.tgdriverapp.models.RideRequests;
import com.xeme.tgdriverapp.models.direction.Direction;
import com.xeme.tgdriverapp.models.direction.Step;
import com.xeme.tgdriverapp.services.MyLocationService;
import com.xeme.tgdriverapp.ui.dialog.MyProgressDialog;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MyHelper;
import com.xeme.tgdriverapp.utils.MySharedPreferences;
import com.xeme.tgdriverapp.utils.SlimApi;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.xeme.tgdriverapp.utils.MyConsta.DoAction_GPS_Enabled;
import static com.xeme.tgdriverapp.utils.MyHelper.Logwtf;
import static com.xeme.tgdriverapp.utils.MyHelper.Snack_Red;

/**
 * A placeholder fragment containing a simple view.
 */
public class RideRequestActivityFragment extends Fragment implements OnMapReadyCallback, LocationListener {

    private final int BOUND_PADDING = 100;
    @BindView(R.id.frr_map)
    MapView mapView;
    @BindView(R.id.content_ride_request)
    FrameLayout contentRideRequest;
    GoogleMap map;
    MyProgressDialog mMyProgressDialog;
    @Inject
    MySharedPreferences mMyPref;
    @Inject
    Retrofit retrofit;
    @Inject
    EventBus mEventBus;

    RideRequests mRequests;
    RideRequestActivity mRequestActivity;
    @BindView(R.id.frr_actv_startride)
    AppCompatButton frrActvStartride;
    @BindView(R.id.frr_actv_cancel)
    AppCompatButton frrActvCancel;

    public RideRequestActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApp) getActivity().getApplication()).getNetComponent().inject(this);
        mEventBus.register(this);
        startLocationUpdates();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ride_request, container, false);
        ButterKnife.bind(this, view);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        mMyProgressDialog = new MyProgressDialog(getContext());
        mRequestActivity = (RideRequestActivity) getActivity();
        mRequests = mRequestActivity.getRequests();

        Logwtf(getClass().getSimpleName() + " PickUp Area", mRequests.pickupArea);
        try {
            MapsInitializer.initialize(this.getActivity());

        } catch (Exception e) {
            e.printStackTrace();
        }



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }

    }


    private void startLocationUpdates() {
        try {


            if (MyLocationService.mGoogleApiClient != null) {
                MyLocationService.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        MyLocationService.mGoogleApiClient);
                if (MyLocationService.mGoogleApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(
                            MyLocationService.mGoogleApiClient, MyLocationService.mLocationRequest, this);
                }
            } else {
                Log.wtf(MyConsta.TAG, "********GoogLeApi Null******* :");
            }

        } catch (SecurityException e) {

            e.printStackTrace();
        }

    }


    private void stopLocationUpdates() {
        try {


            if (MyLocationService.mGoogleApiClient != null) {
                MyLocationService.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        MyLocationService.mGoogleApiClient);

                if (MyLocationService.mGoogleApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(
                            MyLocationService.mGoogleApiClient, this);
                }
            } else {
                Log.wtf(MyConsta.TAG, "********GoogLeApi Null******* :");
            }


        } catch (SecurityException e) {

            e.printStackTrace();
        }
    }


    @Override
    public void onDestroy() {
        stopLocationUpdates();
        super.onDestroy();
        mapView.onDestroy();
        mEventBus.unregister(this);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();

    }

    @Override
    public void onLocationChanged(Location location) {


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        try {
            startLocationUpdates();
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.setMyLocationEnabled(true);
        } catch (SecurityException e) {

            e.printStackTrace();
        }

        InitRoute();


    }


    void InitRoute() {


        map.addMarker(new MarkerOptions().position(new LatLng(22.9776151,72.462755)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_person_pin_circle_primary)).title(mRequests.name).snippet(mRequests.mobile));

        if (MyLocationService.mCurrentLocation != null) {

            String mOrigin = String.valueOf(MyLocationService.mCurrentLocation.getLatitude()) + "," + String.valueOf(MyLocationService.mCurrentLocation.getLongitude());
            String mDestination = mRequests.sourceLatitude+","+mRequests.desLongitude ;
            PrintPoly(mOrigin, mDestination);
        } else {
            MyHelper.ShowSnackBar(mRequestActivity.getRraClMain(), "Please Enable Gps To See Route to reach customer", Snack_Red);
            //map.addMarker(new MarkerOptions().position(mLatLngs.get(mLatLngs.size() - 1)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_dot_pd_72px)));
            //  PrintPoly(Origin,"22.9776151,72.462755");
        }
    }

    void PrintPoly(@Nullable String Origin, @Nullable String Destination) {


        mMyProgressDialog.ShowDialog("Getting Polyline... Please wait");
        Uri.Builder b = Uri.parse(MyConsta.URL_GET_DIRECTIONS).buildUpon();
        // b.appendQueryParameter("origin", "22.9776151,72.462755");
        // b.appendQueryParameter("destination", "23.2061153,72.5592948");
        b.appendQueryParameter("origin", Origin);
        b.appendQueryParameter("destination", Destination);
        b.appendQueryParameter("key", MyConsta.API_Directions);
        Logwtf("PrintPoly ", "URL" + b.toString());
        SlimApi mSlimApi = retrofit.create(SlimApi.class);
        Observable<Direction> mDirection = mSlimApi.Get_Direction(b.toString());
        PolylineOptions options = new PolylineOptions();
        options.color(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        options.geodesic(true);
        options.zIndex(1000f);
        options.width(16f);
        mDirection.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Direction>() {
                    @Override
                    public void onCompleted() {
                        Handler handler = new Handler();
                        handler.post(() -> mMyProgressDialog.hideDialog());


                    }

                    @Override
                    public void onError(Throwable e) {
                        Handler handler = new Handler();
                        handler.post(() -> mMyProgressDialog.hideDialog());
                        Logwtf("AcceptReq", "onError" + e.getCause());

                    }

                    @Override
                    public void onNext(Direction direction) {
                        if (!direction.status.equals("ok")) {
                            map.clear();
                            List<LatLng> mLatLngs = new ArrayList<>();
                            for (Step step : direction.routes.get(0).legs.get(0).steps) {
                                mLatLngs.addAll(MyHelper.decodePoly(step.polyline.points));
                            }


                            options.addAll(mLatLngs);
                            new Handler().post(() -> {

                                if (mLatLngs.size() > 1) {
                                    map.addMarker(new MarkerOptions().position(mLatLngs.get(0)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_dot_pd_72px)).flat(true).anchor(0.5f, 0.5f));
                                    map.addMarker(new MarkerOptions().position(mLatLngs.get(mLatLngs.size() - 1)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_dot_pd_72px)).flat(true).anchor(0.5f, 0.5f));
                                    map.addMarker(new MarkerOptions().position(mLatLngs.get(mLatLngs.size() - 1)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_person_pin_circle_primary)).title(mRequests.name).snippet(mRequests.mobile));

                                }
                                map.addPolyline(options);
                            });

                            LatLngBounds.Builder builder = new LatLngBounds.Builder();


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                mLatLngs.forEach(builder::include);
                            } else {
                                for (LatLng latLng : mLatLngs) {
                                    builder.include(latLng);
                                }
                            }
                            final LatLngBounds bounds = builder.build();
                            //BOUND_PADDING is an int to specify padding of bound.. try 100.
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, BOUND_PADDING);
                            map.animateCamera(cu);

                        }
                    }
                });
    }

    @Subscribe
    public void DoActionRRAF(DoAction mDoAction) {
        if (mDoAction.DoWhat == DoAction_GPS_Enabled) {
            InitRoute();
        }
        /*if (mDoAction.DoWhat == DoAction_GoogleApiDisconnected) {

        }*/
    }

    @OnClick({R.id.frr_actv_startride, R.id.frr_actv_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.frr_actv_startride:
                YoYo.with(Techniques.Pulse).duration(200).playOn(frrActvStartride);
                MyHelper.showPopUpdialog(128, getContext(), "Start Ride", "Are you sure?", "Yes", "No", false);

                break;
            case R.id.frr_actv_cancel:
                YoYo.with(Techniques.Pulse).duration(200).playOn(frrActvCancel);
                MyHelper.showPopUpdialog(131, getContext(), "Cancel Ride & exit?", "You want to cancel this Ride and exit. Are you sure?", "Yes", "No", false);
                break;
        }
    }


    @Subscribe
    public void DialogResult(DialogResult mResult) {

        if (mResult.getForWho() == 128) {

            if (mResult.isResult()) {
                Toast.makeText(mRequestActivity, "Reached", Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new DoAction(132));
            } else {
                Toast.makeText(mRequestActivity, "Not now", Toast.LENGTH_SHORT).show();
            }
        }

    }
}
