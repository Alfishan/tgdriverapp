package com.xeme.tgdriverapp.ui.activity;

import android.content.Intent;
import android.location.GpsStatus;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.animation.Animation;

import com.afollestad.assent.AfterPermissionResult;
import com.afollestad.assent.Assent;
import com.afollestad.assent.AssentCallback;
import com.afollestad.assent.PermissionResultSet;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.xeme.tgdriverapp.MyApp;
import com.xeme.tgdriverapp.R;
import com.xeme.tgdriverapp.models.DialogResult;
import com.xeme.tgdriverapp.services.MyLocationService;
import com.xeme.tgdriverapp.services.MySyncService;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MyHelper;
import com.xeme.tgdriverapp.utils.MySharedPreferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.xeme.tgdriverapp.utils.MyHelper.Logwtf;

public class SplashActivity extends AppCompatActivity implements Animation.AnimationListener, AssentCallback, GpsStatus.Listener {

    /*  @Inject
      Retrofit retrofit;*/
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    @BindView(R.id.sa_aciv_logo)
    AppCompatImageView saAcivLogo;
    @Inject
    MySharedPreferences mMyPref;

    @Inject
    EventBus mEventBus;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient mClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Assent.setActivity(this, this);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        mEventBus.register(this);


        /*try {
            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            lm.addGpsStatusListener(this);

        }

        catch (SecurityException e)
        {

        }    */    ButterKnife.bind(this);
        Logwtf("FireBase Token", FirebaseInstanceId.getInstance().getToken());
/*
        Animation AimFadeIN = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.fadein);
        AimFadeIN.setAnimationListener(this);
         saAcivLogo.startAnimation(AimFadeIN);
        */
        YoYo.with(Techniques.Tada).duration(1000).playOn(saAcivLogo);


        checkpermission();


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mClient = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    void checkpermission() {

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {

            if (!Assent.isPermissionGranted(Assent.ACCESS_FINE_LOCATION)) {
                Assent.requestPermissions(SplashActivity.this, 69,
                        Assent.ACCESS_FINE_LOCATION);
            } else {

                init();
            }

        } else {
            init();
        }
    }

    private void init() {

        if (checkPlayServices()) {

            mMyPref.putBoolean(MyConsta.Settings_Has_PlayServices,true);
            new Handler().postDelayed(this::Redirect, 2000);
        } else {
            mMyPref.putBoolean(MyConsta.Settings_Has_PlayServices,false);
            MyHelper.ShowSnackBar(this.getWindow().getDecorView(), "Please Install Latest Google Play Service To Use this App.", MyHelper.Snack_Red);
        }

    }

    void Redirect() {

        Intent LoginIntent = new Intent(SplashActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Intent HomeIntent = new Intent(SplashActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        long CurrentTime = Calendar.getInstance().getTimeInMillis();

        Logwtf("Redirect():", "IsloggedIn" + mMyPref.getBoolean(MyConsta.KEY_IsLoggedIn, false) + " PID:" + mMyPref.getInt(MyConsta.KEY_MYPID, 0) + " Apikey:" + mMyPref.getString(MyConsta.KEY_UUID, MyConsta.KEY_Default_Value));
        if (mMyPref.getBoolean(MyConsta.KEY_IsLoggedIn, false) && mMyPref.getInt(MyConsta.KEY_MYPID, 0) > 0 && !mMyPref.getString(MyConsta.KEY_UUID, MyConsta.KEY_Default_Value).equals("")) {

            Logwtf("Redirect()", "now Checking Time");
            if (mMyPref.getLong(MyConsta.KEY_IsLoggedInTime, 0) < CurrentTime) {

            } else {

                MyHelper.ShowSnackBar(this.getWindow().getDecorView(), "Please Adjust Your DATE & TIME.Or try to Login Again.", MyHelper.Snack_Red);
                Logwtf("Redirect()", "now Checking Time:Time Does not match");
                // startActivity(LoginIntent);
                //finish();
            }
            GoToHome();
        } else {
            Logwtf("Redirect()", "no Login Info Found");
            startActivity(LoginIntent);
            finish();
        }

    }


    void GoToHome(){

        startActivity(new Intent(this,HomeActivity.class));
        Intent LocationService = new Intent(SplashActivity.this, MyLocationService.class);
        startService(LocationService);

        Intent SyncService = new Intent(this, MySyncService.class).setAction(MySyncService.SYNC_All);
        startService(SyncService);
        Logwtf("Redirect()", "now Checking Time:All ok");
        finish();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        // saAcivLogo.startAnimation(AnimationUtils.loadAnimation(SplashActivity.this, R.anim.shake));
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Logwtf("checkPlayServices", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        // Updates the activity every time the Activity becomes visible again
        Assent.setActivity(this, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Cleans up references of the Activity to avoid memory leaks
        if (isFinishing())
            Assent.setActivity(this, null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Lets Assent handle permission results, and contact your callbacks
        Assent.handleResult(permissions, grantResults);
    }

    @AfterPermissionResult(permissions = {Assent.ACCESS_FINE_LOCATION})
    public void onPermissionResult(PermissionResultSet result) {
        // // Use PermissionResultSet
        if (result.allPermissionsGranted()) {
            init();
        } else {
            MyHelper.showPopUpdialog(12, this, "Permission", "App need some permission to run.Do you want to retry?", "Yes", "No", false);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
    }


    @Subscribe
    public void DialogResult(DialogResult mResult) {

        if (mResult.getForWho() == 12) {

            if (mResult.isResult()) {
                checkpermission();
            } else {
                finish();
            }
        }


        if (mResult.getForWho() == 10) {

            if (mResult.isResult()) {
                MyHelper.ShowSnackBar(this.getWindow().getDecorView(), "Gps Enabled", MyHelper.Snack_Green);

            } else {
                MyHelper.ShowSnackBar(this.getWindow().getDecorView(), "GPS_EVENT_STOPPED", MyHelper.Snack_Red);

            }
        }
    }

    @Override
    public void onGpsStatusChanged(int i) {
        switch (i) {

            case GpsStatus.GPS_EVENT_STARTED:
                MyHelper.ShowSnackBar(this.getWindow().getDecorView(), "Gps Enabled", MyHelper.Snack_Green);
                break;
            case GpsStatus.GPS_EVENT_STOPPED:
                MyHelper.ShowSnackBar(this.getWindow().getDecorView(), "GPS_EVENT_STOPPED", MyHelper.Snack_Red);
                break;
            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
             //   MyHelper.ShowSnackBar(this.getWindow().getDecorView(), "GPS_EVENT_SATELLITE_STATUS", MyHelper.Snack_Red);
                break;
            case GpsStatus.GPS_EVENT_FIRST_FIX:
               // MyHelper.ShowSnackBar(this.getWindow().getDecorView(), "Gps Fix", MyHelper.Snack_Green);
                break;
        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Splash Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mClient.connect();
        AppIndex.AppIndexApi.start(mClient, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(mClient, getIndexApiAction());
        mClient.disconnect();
    }
}
