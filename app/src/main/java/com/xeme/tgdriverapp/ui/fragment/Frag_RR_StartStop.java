package com.xeme.tgdriverapp.ui.fragment;


import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.xeme.tgdriverapp.MyApp;
import com.xeme.tgdriverapp.R;
import com.xeme.tgdriverapp.models.DialogResult;
import com.xeme.tgdriverapp.models.DoAction;
import com.xeme.tgdriverapp.models.RideRequests;
import com.xeme.tgdriverapp.models.direction.Direction;
import com.xeme.tgdriverapp.models.direction.Step;
import com.xeme.tgdriverapp.services.MyLocationService;
import com.xeme.tgdriverapp.ui.activity.RideRequestActivity;
import com.xeme.tgdriverapp.ui.dialog.MyProgressDialog;
import com.xeme.tgdriverapp.utils.MyConsta;
import com.xeme.tgdriverapp.utils.MyHelper;
import com.xeme.tgdriverapp.utils.MySharedPreferences;
import com.xeme.tgdriverapp.utils.SlimApi;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.xeme.tgdriverapp.utils.MyConsta.DoAction_GPS_Enabled;
import static com.xeme.tgdriverapp.utils.MyHelper.Logwtf;
import static com.xeme.tgdriverapp.utils.MyHelper.Snack_Red;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_RR_StartStop extends Fragment implements OnMapReadyCallback, LocationListener {


    @BindView(R.id.frrss_map)
    MapView mapView;
    @BindView(R.id.frrss_actv_action1)
    AppCompatButton frrssActvAction1;
    @Inject
    MySharedPreferences mMyPref;
    @Inject
    Retrofit retrofit;
    GoogleMap map;
    MyProgressDialog mMyProgressDialog;
    @Inject
    EventBus mEventBus;
    RideRequests mRequests;
    RideRequestActivity mRequestActivity;

    public Frag_RR_StartStop() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApp) getActivity().getApplication()).getNetComponent().inject(this);
        mEventBus.register(this);
        mMyProgressDialog = new MyProgressDialog(getContext());
        startLocationUpdates();
    }


    private void startLocationUpdates() {
        try {


            if (MyLocationService.mGoogleApiClient != null) {
                MyLocationService.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        MyLocationService.mGoogleApiClient);
                if (MyLocationService.mGoogleApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(
                            MyLocationService.mGoogleApiClient, MyLocationService.mLocationRequest, this);
                }
            } else {
                Log.wtf(MyConsta.TAG, "********GoogLeApi Null******* :");
            }

        } catch (SecurityException e) {

            e.printStackTrace();
        }

    }


    private void stopLocationUpdates() {
        try {


            if (MyLocationService.mGoogleApiClient != null) {
                MyLocationService.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        MyLocationService.mGoogleApiClient);

                if (MyLocationService.mGoogleApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(
                            MyLocationService.mGoogleApiClient, this);
                }
            } else {
                Log.wtf(MyConsta.TAG, "********GoogLeApi Null******* :");
            }


        } catch (SecurityException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        stopLocationUpdates();
        super.onDestroy();
        mapView.onDestroy();
        mEventBus.unregister(this);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();

    }

    @Override
    public void onLocationChanged(Location location) {


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        try {
            startLocationUpdates();
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.setMyLocationEnabled(true);
        } catch (SecurityException e) {

            e.printStackTrace();
        }

        InitRoute();


    }

    void InitRoute() {


        if ((!mRequests.sourceLatitude.equals("") && !mRequests.sourceLongitude.equals("")) && (!mRequests.desLatitude.equals("") && !mRequests.desLongitude.equals(""))) {

            String Origin = mRequests.sourceLatitude + "," + mRequests.sourceLongitude;//String.valueOf(MyLocationService.mCurrentLocation.getLatitude()) + "," + String.valueOf(MyLocationService.mCurrentLocation.getLongitude());
            PrintPoly(Origin, mRequests.desLatitude + "," + mRequests.desLongitude);
        } else {
            MyHelper.ShowSnackBar(mRequestActivity.getRraClMain(), "Please Enable Gps To See Route.", Snack_Red);
            //map.addMarker(new MarkerOptions().position(mLatLngs.get(mLatLngs.size() - 1)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_dot_pd_72px)));
            //  PrintPoly(Origin,"22.9776151,72.462755");
        }

        Addmarkers();
         }


    void Addmarkers(){


        if (!mRequests.sourceLatitude.equals("") && !mRequests.sourceLongitude.equals(""))
            map.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(mRequests.sourceLatitude), Double.valueOf(mRequests.sourceLongitude))).title("Origin").snippet(mRequests.pickupArea));

        if (!mRequests.desLatitude.equals("") && !mRequests.desLongitude.equals(""))
            map.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(mRequests.desLatitude), Double.valueOf(mRequests.desLongitude))).title("Destination").snippet(mRequests.dropArea));

    }


    void PrintPoly(@Nullable String Origin, @Nullable String Destination) {


        mMyProgressDialog.ShowDialog("Getting Polyline... Please wait");
        Uri.Builder b = Uri.parse(MyConsta.URL_GET_DIRECTIONS).buildUpon();
        // b.appendQueryParameter("origin", "22.9776151,72.462755");
        // b.appendQueryParameter("destination", "23.2061153,72.5592948");
        b.appendQueryParameter("origin", Origin);
        b.appendQueryParameter("destination", Destination);
        b.appendQueryParameter("key", MyConsta.API_Directions);
        Logwtf("AcceptReq", "URL" + b.toString());
        SlimApi mSlimApi = retrofit.create(SlimApi.class);
        Observable<Direction> mDirection = mSlimApi.Get_Direction(b.toString());
        PolylineOptions options = new PolylineOptions();
        options.color(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        options.geodesic(true);
        options.zIndex(1000f);
        options.width(16f);
        mDirection.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Direction>() {
                    @Override
                    public void onCompleted() {
                        Handler handler = new Handler();
                        handler.post(() -> mMyProgressDialog.hideDialog());


                    }

                    @Override
                    public void onError(Throwable e) {
                        Handler handler = new Handler();
                        handler.post(() -> mMyProgressDialog.hideDialog());
                        Logwtf("AcceptReq", "onError" + e.getCause());

                    }

                    @Override
                    public void onNext(Direction direction) {
                        if (!direction.status.equals("ok")) {
                            map.clear();
                            List<LatLng> mLatLngs = new ArrayList<>();
                            for (Step step : direction.routes.get(0).legs.get(0).steps) {
                                mLatLngs.addAll(MyHelper.decodePoly(step.polyline.points));
                            }

                            options.addAll(mLatLngs);
                            new Handler().post(() -> {

                                if (mLatLngs.size() > 1) {
                                    map.addMarker(new MarkerOptions().position(mLatLngs.get(0)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_dot_pd_72px)).flat(true).anchor(0.5f, 0.5f));
                                    map.addMarker(new MarkerOptions().position(mLatLngs.get(mLatLngs.size() - 1)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_dot_pd_72px)).flat(true).anchor(0.5f, 0.5f));
/*
                                    map.addMarker(new MarkerOptions().position(mLatLngs.get(0)).title("Origin").snippet(mRequests.pickupArea));
                                    map.addMarker(new MarkerOptions().position(mLatLngs.get(mLatLngs.size() - 1)).title("Destination").snippet(mRequests.dropArea));
*/

                                }
                                map.addPolyline(options);
                                Addmarkers();
                            });

                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            for (LatLng latLng : mLatLngs) {
                                builder.include(latLng);
                            }
                            final LatLngBounds bounds = builder.build();
                            //BOUND_PADDING is an int to specify padding of bound.. try 100.
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 100);
                            map.animateCamera(cu);

                        }
                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frag_rr_startstop, container, false);
        ButterKnife.bind(this, view);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        mRequestActivity = (RideRequestActivity) getActivity();
        mRequests = mRequestActivity.getRequests();

        Logwtf(getClass().getSimpleName() + "PickUp Area", mRequests.pickupArea);
        try {
            MapsInitializer.initialize(this.getActivity());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }

    }

    @OnClick(R.id.frrss_actv_action1)
    public void onClick() {
        YoYo.with(Techniques.Pulse).duration(200).playOn(frrssActvAction1);
        MyHelper.showPopUpdialog(132, getContext(), "Stop Ride.", "You want to stop this Ride. Are you sure?", "Yes", "No", false);
    }

    @Subscribe
    public void DialogResult(DialogResult mResult) {

        if (mResult.getForWho() == 132) {

            if (mResult.isResult()) {
                EventBus.getDefault().post(new DoAction(101));
                //EventBus.getDefault().post(new DoAction(132));
            } else {
                Toast.makeText(mRequestActivity, "OK", Toast.LENGTH_SHORT).show();
            }
        }

       /* if (mResult.getForWho() == 129) {

            if (mResult.isResult()) {
                Toast.makeText(mRequestActivity, "Canceled", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mRequestActivity, "No i don't", Toast.LENGTH_SHORT).show();
            }
        }*/

    }


    @Subscribe(threadMode = ThreadMode.POSTING)
    public void DoActionRRAF(DoAction mDoAction) {
        if (mDoAction.DoWhat == DoAction_GPS_Enabled) {
            InitRoute();
        }

        /*if (mDoAction.DoWhat == DoAction_GoogleApiDisconnected) {

        }*/
    }
}
