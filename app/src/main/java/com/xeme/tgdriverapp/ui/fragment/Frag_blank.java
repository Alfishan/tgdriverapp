package com.xeme.tgdriverapp.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xeme.tgdriverapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_blank extends Fragment {


    public Frag_blank() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.frag_rr_startstop, container, false);
    }

}
