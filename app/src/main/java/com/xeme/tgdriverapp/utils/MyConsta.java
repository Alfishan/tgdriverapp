package com.xeme.tgdriverapp.utils;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;

/**
 * Created by root on 10/8/16.
 */
public class MyConsta {
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat mDateFormat = new SimpleDateFormat("dd/MM/yy hh:mm a");
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat mDateFormatOnlyTime = new SimpleDateFormat("hh:mm a");
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat mDateFormatOnlyDate = new SimpleDateFormat("dd/MM/yy");
    public static final String My_Pref = "TGDriverApp";
    public static final String TAG = "TGDriverApp";
    //Api URLS
    public static final String URL_BASE = "http://travelgujju.com";
    public static final String URL_LOGIN = "tm/v1/login";
    public static final String URL_RideRequests = "tm/v1/caborderlist";
    public static final String URL_AcceptRideRequests = "tm/v1/acceptorder";
    public static final String URL_UPDATE_FBT = "tm/v1/updatefirebasetoken";
    public static final String URL_Past_orders = "tm/v1/orderhistory";

    public static final String URL_Driver_Status = "tm/v1/driveractive";










    public static final String Default_Profile =URL_BASE+"/demo/healink/uploads/default.png";


    public static final String API_Directions="AIzaSyD62OU0rMb2djpj47E77Pc-u76QGM8UVOY";
    public static final String URL_GET_DIRECTIONS="https://maps.googleapis.com/maps/api/directions/json";
    //DoAction;

    public static final int DoAction_SYNC_Fitbit=787445;
    public static final int DoAction_GetFitbitToken=99974;

    public static final int DoAction_SyncHOme=123;
    public static final int DoAction_SyncRideHistory=102;

    public static final int DoAction_GoogleApiConnected=124;
    public static final int DoAction_GoogleApiDisconnected=125;
    public static final int DoAction_GPS_Enabled=126;
    public static final int DoAction_GPS_Stopped=127;
    public static final int DoAction_Refresh_RideHistory=100;

    //Treatment Type in int

    public static final int TREATMENT_TYPE_VITALS=1;
    public static final int TREATMENT_TYPE_CHECKINS=2;
    public static final int TREATMENT_TYPE_MEDICATION=3;
    public static final int TREATMENT_TYPE_APPOINTMENT=4;
    public static final int TREATMENT_TYPE_RECOMMENDATION=5;

    //Treatment Name
    public static final String TREATMENT_VITALS="Vitals";
    public static final String TREATMENT_CHECKINS="Check Ins";
    public static final String TREATMENT_MEDICATION="Medications";
    public static final String TREATMENT_APPOINTMENT="Appointments";
    public static final String TREATMENT_RECOMMENDATION="Recommendations";
    //Treatment Status

    public static final int TREATMENT_STATUS_MISSED=0;
    public static final int TREATMENT_STATUS_DONE=1;

//UserType

    public static final int USERTYPE_MANAGER=2;
    public static final int USERTYPE_DOCTOR=3;
    public static final int USERTYPE_ADMIN=1;

    //Keys
    public static final String KEY_UUID = "UUID";
    public static final String KEY_IsLoggedIn = "IsLoggedIn";
    public static final String KEY_KeepMeLoggedIn = "KeepMeLoggedIn";
    public static final String KEY_Email = "email";
    public static final String KEY_Name = "name";
    public static final String KEY_Surname = "surname";
    public static final String KEY_FirstName = "name";
    public static final String KEY_Lastname = "surname";
    public static final String KEY_FullName = "FullName";
    public static final String KEY_NickName = "FullName";
    public static final String KEY_ProfileImage = "ProfileImage";
    public static final String KEY_City = "City";
    public static final String KEY_State = "State";
    public static final String KEY_PostalCode = "PostalCode";
    public static final String KEY_Phone = "Phone";
    public static final String KEY_Phone1 = "Phone1";
    public static final String KEY_Phone2 = "Phone2";
    public static final String KEY_Address = "Address";
    public static final String KEY_Default_Value = "";
    public static final int RESPONSE_OK = 200;
    public static final int BAD_REQUEST = 201;
    public static final int RESPONSE_MISSING = 203;
    public static final int RESPONSE_SERVER_ERROR = 500;

    public static final String KEY_CurrentBalance = "CurrentBalance";
    public static final String KEY_ProofType = "ProofType";
    public static final String KEY_ProofNumber = "ProofNumber";

    public static final String KEY_USER_ACCOUNT = "UserAccount";

    public static final int[] KEY_Gender_Value={0,1};
    public static final String KEY_GENDER = "UserGender";
    public static final String KEY_GENDER_MALE = "male";
    public static final String KEY_GENDER_FEMALE = "female";

    public static final String KEY_DOB = "DateOfBirth";
    public static final String KEY_BLOODGROUP = "BloodGroup";
    public static final String KEY_DISTRICT = "District";
    public static final String KEY_COUNTRY = "Country";

    public static final String KEY_TREATMENT_TYPE = "Treatment_type";

    public static final String KEY_ISMEDICAL_INSURANCE = "Medical_Insurance";

    public static final String KEY_MEDICAL_INSURANCE_NAME = "Medical_Insurance_Name";
    public static final String KEY_MEDICAL_INSURANCE_NUMBER = "Medical_Insurance_Number";

    public static final String KEY_WEIGHT ="Weight" ;

    public static final String KEY_ALLERGIES = "Allergies";
    public static final String KEY_ALLERGIES_INFO = "Allergies_Info";
    public static final String KEY_MYPID = "MyPid";
    public static final String KEY_HEIGHT ="Weight" ;
    public static final String KEY_TREATMENT_INFO ="Treatment_info" ;
    public static final String KEY_DOCTOR_ID ="DOCTOR_ID" ;
    public static final String KEY_IsLoggedInTime="LoginTime";

    public static final String KEY_FBTOKEN ="FBTOKEN" ;
    public static final String KEY_IsInternet="IsInternet";

    public static final String KEY_RID="RID";

//Ride Status

    public static final int Ride_Status_Processing=0;
    public static final int Ride_Status_Booking=1;
    public static final int Ride_Status_Cancel=2;
    public static final int Ride_Status_Complete=3;
    public static final int Ride_Status_Start_trip=4;

    //Settings Key
    public static final String Settings_NotificationIsSet = "NotificationSet";
    public static final String Settings_NotificationURI = "NotificationURI";
    public static final String Settings_NotificationPath = "NotificationPath";
    public static final String Settings_NotificationTitle = "NotificationTitle";
    public static final String Settings_NotificationVibration = "NotificationVibration";
    public static final int Settings_NotificationVibration_Off = 0;
    public static final int Settings_NotificationVibration_Default = 1;
    public static final int Settings_NotificationVibration_Short = 2;
    public static final int Settings_NotificationVibration_Long = 3;

    //Vibration Pattern
    private static final int dot = 200;      // Length of a Morse Code "dot" in milliseconds
    private static final int dash = 500;     // Length of a Morse Code "dash" in milliseconds
    private static final int short_gap = 200;    // Length of Gap Between dots/dashes
    private static final int medium_gap = 500;   // Length of Gap Between Letters
    private static final int long_gap = 1000;    // Length of Gap Between Words
    public static final long[] pattern = {
            0,  // Start immediately
            dot, short_gap, dot, short_gap, dot,    // s
            medium_gap,
            dash, short_gap, dash, short_gap, dash, // o
            medium_gap,
            dot, short_gap, dot, short_gap, dot,    // s
            long_gap
    };
    public static final long[] Virbation_Short = {0, dot, short_gap, dot, short_gap, dot, medium_gap,};
    public static final long[] Virbation_Default = {0, long_gap};
    public static final long[] Virbation_Long = {0,
            dot, short_gap, dot, short_gap, dot,
            medium_gap,
            dash, short_gap, dash, short_gap, dash,
            medium_gap,
            dot, short_gap, dot, short_gap, dot,
            long_gap};


    public static final String Settings_NotificationLight = "NotificationLight";
    public static final int Settings_NotificationLight_None = 0;
    public static final int Settings_NotificationLight_White = 1;
    public static final int Settings_NotificationLight_Red = 2;
    public static final int Settings_NotificationLight_Yellow = 3;
    public static final int Settings_NotificationLight_Green = 4;
    public static final int Settings_NotificationLight_Cyan = 5;
    public static final int Settings_NotificationLight_Blue = 6;
    public static final int Settings_NotificationLight_Purple = 7;



    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 60000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;



    public static final String KEY_FIREBASE_USER_UID ="FIREBASE_USER_UID" ;

    ///Font  ///TypeFace


    public static final int Font_Light=1;
    public static final int Font_Thin=2;

    public static final String Settings_Has_PlayServices = "Has_PlayServices";

}
