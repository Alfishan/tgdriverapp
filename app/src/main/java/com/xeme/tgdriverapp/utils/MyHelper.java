package com.xeme.tgdriverapp.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import com.google.android.gms.maps.model.LatLng;
import com.xeme.tgdriverapp.models.DialogResult;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import static com.xeme.tgdriverapp.utils.MyConsta.mDateFormat;
import static com.xeme.tgdriverapp.utils.MyConsta.mDateFormatOnlyTime;

/**
 * Created by root on 31/8/16.
 */
public class MyHelper {
    private static int screenWidth = 0;
    private static int screenHeight = 0;

    public static void Logwtf(String From, String msg) {

        Log.wtf(MyConsta.TAG, "#-#-#From:" + From + " Log:" + msg);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int getScreenHeight(Context c) {
        if (screenHeight == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenHeight = size.y;
        }

        return screenHeight;
    }

    public static int getScreenWidth(Context c) {
        if (screenWidth == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        }

        return screenWidth;
    }

    public static boolean isAndroid5() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        //calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String getDate() {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = MyConsta.mDateFormatOnlyDate;

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        return formatter.format(calendar.getTimeInMillis());
    }

    public static String getDateTimeAsString(String Miliseconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = mDateFormat;

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.valueOf(Miliseconds));
        return formatter.format(calendar.getTimeInMillis());
    }

    public static String GetformatToYesterdayOrToday(Long TimeMili) {
        Calendar TimeNow = Calendar.getInstance();
        TimeNow.setTimeZone(TimeZone.getTimeZone("GMT"));
        Calendar TimeToFormat = Calendar.getInstance();
        TimeToFormat.setTimeInMillis(TimeMili);
        TimeToFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat Formater = mDateFormatOnlyTime;
        Formater.setTimeZone(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat Formater2 = mDateFormat;
        Formater2.setTimeZone(TimeZone.getTimeZone("GMT"));
      /* if (DateUtils.isToday(TimeMili)){


       }*/

        if (TimeNow.get(Calendar.YEAR) == TimeToFormat.get(Calendar.YEAR) && TimeNow.get(Calendar.DAY_OF_YEAR) == TimeToFormat.get(Calendar.DAY_OF_YEAR)) {
            return "Today " + Formater.format(TimeMili);
        } else if (TimeNow.get(Calendar.YEAR) == TimeToFormat.get(Calendar.YEAR) && TimeNow.get(Calendar.DAY_OF_YEAR) - TimeToFormat.get(Calendar.DAY_OF_YEAR) ==1) {
            return "Yesterday " + Formater.format(TimeMili);
        } else if (TimeNow.get(Calendar.YEAR) == TimeToFormat.get(Calendar.YEAR) && TimeNow.get(Calendar.DAY_OF_YEAR) - TimeToFormat.get(Calendar.DAY_OF_YEAR)==-1) {
            return "Tomorrow " + Formater.format(TimeMili);
        } else {
            return Formater2.format(TimeMili);
        }


    }


    public  static boolean GetCanTake(Long TimeInMilis){
        Calendar TimeNow = Calendar.getInstance();
        TimeNow.setTimeZone(TimeZone.getTimeZone("GMT"));
        Calendar TimeToCompare = Calendar.getInstance();
        TimeToCompare.setTimeInMillis(TimeInMilis);
        TimeToCompare.setTimeZone(TimeZone.getTimeZone("GMT"));
        if (TimeNow.get(Calendar.YEAR) == TimeToCompare.get(Calendar.YEAR) && TimeNow.get(Calendar.DAY_OF_YEAR) == TimeToCompare.get(Calendar.DAY_OF_YEAR)) {
            TimeNow.set(Calendar.HOUR_OF_DAY,TimeNow.get(Calendar.HOUR_OF_DAY)+1);
            if (TimeNow.get(Calendar.HOUR_OF_DAY)>=TimeToCompare.get(Calendar.HOUR_OF_DAY)){
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    public static final int Snack_Red=1;
    public static final int Snack_Green=2;
    public static final int Snack_White=3;
    public static final String  Snack_msg_NoInternet="No internet Connection Available. Make sure you have a working internet connection.";
    public static void ShowSnackBar(View view,String msg,int WhatKindOfSnack){



        switch (WhatKindOfSnack) {

            case Snack_Red:
                Snackbar.make( view, Html.fromHtml("<font color=\"#FF4436\">"+msg+"</font>"), Snackbar.LENGTH_LONG ).show();
                break;
            case Snack_Green:
                Snackbar.make( view, Html.fromHtml("<font color=\"#00B147\">"+msg+"</font>"), Snackbar.LENGTH_LONG ).show();
                break;
            case Snack_White:
                Snackbar.make( view,msg, Snackbar.LENGTH_LONG ).show();
                break;
        }
    }

   public static  long  getTimeNow() {
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("IST"));
        return c.getTimeInMillis();
    }



    public static void showPopUpdialog(int DialogOf,Context ctx, String Title, String msg, String Positive, String Negative, boolean isCancelable) {

       android.support.v7.app.AlertDialog.Builder mDialog= new android.support.v7.app.AlertDialog.Builder(ctx);
   /*setIcon(R.mipmap.ic_launcher)*/
        mDialog.setTitle(Title)
                .setCancelable(isCancelable)
                .setMessage(msg)
                .setPositiveButton(Positive, (dialog, which) -> EventBus.getDefault().post(new DialogResult(DialogOf,true)))
                .setNegativeButton(Negative, (dialog, id) -> EventBus.getDefault().post(new DialogResult(DialogOf,false)));
        mDialog.show();
    }

    public static boolean isGnssStatusListenerSupported() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.N;
    }

    public static List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

}


