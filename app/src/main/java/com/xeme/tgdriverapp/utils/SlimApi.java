package com.xeme.tgdriverapp.utils;


import com.xeme.tgdriverapp.models.LoginResponse;
import com.xeme.tgdriverapp.models.PastOrders;
import com.xeme.tgdriverapp.models.RideRequestMain;
import com.xeme.tgdriverapp.models.Simple_Response;
import com.xeme.tgdriverapp.models.User;
import com.xeme.tgdriverapp.models.direction.Direction;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;
import rx.Observable;


/**
 * Created by root on 28/6/16.
 */
public interface SlimApi {


    @FormUrlEncoded
    @POST(MyConsta.URL_LOGIN)
    Call<User> GetLoginDetails(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST(MyConsta.URL_RideRequests)
    Call<RideRequestMain> GetRideRequests(@Field("did") int did);



    @FormUrlEncoded
    @POST(MyConsta.URL_LOGIN)
    Observable<LoginResponse> GetLoginDetailswithRx(@Field("email") String email, @Field("password") String password);




    @GET
    Observable<Direction> Get_Direction(@Url String DirectionURL);


    @FormUrlEncoded
    @POST(MyConsta.URL_RideRequests)
    Observable<Response<RideRequestMain>> GetRideRequestsRX(@Field("did") int did);


    @FormUrlEncoded
    @POST(MyConsta.URL_AcceptRideRequests)
    Observable<Response<Simple_Response>> PostAcceptOrderRX(@Field("did") int did, @Field("order_id") Long o_id,@Field("status") int status);


    @FormUrlEncoded
    @POST(MyConsta.URL_UPDATE_FBT)
    Call<Simple_Response> PostUpdateFBToken(@Field("apikey") String apikey,
                                            @Field("id") int did, @Field("token") String token);




    @FormUrlEncoded
    @POST(MyConsta.URL_Past_orders)
    Observable<Response<PastOrders>> PostPastOrdersRX(@Field("apikey") String apikey,
                                      @Field("did") int did);

    @FormUrlEncoded
    @POST(MyConsta.URL_Driver_Status)
    Observable<Response<Simple_Response>> PostDriverStatus(@Field("is_active") int IsActive,
                                                      @Field("did") int did);


/*
    @FormUrlEncoded
    @POST(MyConsta.URL_APPOINTMENTS)
    Observable<AppointsmentsMaster> GetAppointmentsRx(@Field("pid") String pid, @Field("apikey") String apikey);

    @FormUrlEncoded
    @POST(MyConsta.URL_APPOINTMENTS)
    Call<AppointsmentsMaster> GetAppointments(@Field("pid") String pid, @Field("apikey") String apikey);

    @FormUrlEncoded
    @POST(MyConsta.URL_VITALS)
    Call<VitalsMaster> GetVitals(@Field("pid") String pid, @Field("apikey") String apikey);

    @FormUrlEncoded
    @POST(MyConsta.URL_RECOMENDATION)
    Call<RecomendationMaster> GetRecommendation(@Field("pid") String pid, @Field("apikey") String apikey);


    @FormUrlEncoded
    @POST(MyConsta.URL_MEDICATION)
    Call<MedicationMaster> GetMedication(@Field("pid") String pid, @Field("apikey") String apikey);

    @FormUrlEncoded
    @POST(MyConsta.URL_CHECKINS)
    Call<CheckinMaster> GetCheckIns(@Field("pid") String pid, @Field("apikey") String apikey);


    @FormUrlEncoded
    @POST(MyConsta.URL_UPDATE_FBT)
    Call<Simple_Response> PostUpdateFBToken(@Field("apikey") String apikey,
                                        @Field("pid") int pid, @Field("token") String token);


    @FormUrlEncoded
    @POST(MyConsta.URL_STATUS_UPDATE_MEDICATION)
    Call<Simple_Response> StatusUpdateMedication(@Field("apikey") String apikey,
                                                 @Field("pid") int pid,
                                                 @Field("taskid") long TaskID,
                                                 @Field("result") int Status);

    @FormUrlEncoded
    @POST(MyConsta.URL_STATUS_UPDATE_APPOINTMENT)
    Call<Simple_Response> StatusUpdateAppointment(@Field("apikey") String apikey,
                                                  @Field("pid") int pid,
                                                  @Field("taskid") long TaskID,
                                                  @Field("result") int Status);


    @FormUrlEncoded
    @POST(MyConsta.URL_SUBMIT_CHECKIN)
    Call<Simple_Response> ResultCheckIn(@Field("apikey") String apikey,
                                        @Field("pid") int pid,
                                        @Field("taskid") long TaskID,
                                        @Field("result") int Status,
                                        @Field("timestamp") String timestamp,
                                        @Field("question") String questions);


    @FormUrlEncoded
    @POST(MyConsta.URL_SUBMIT_VITAL)
    Call<Simple_Response> ResultVItal(@Field("apikey") String apikey,
                                      @Field("pid") int pid,
                                      @Field("taskid") long TaskID,
                                      @Field("bp1") String bp1,
                                      @Field("bp2") String bp2,
                                      @Field("heartbeat") String heartbeat,
                                      @Field("steps") String steps
    );


    @FormUrlEncoded
    @POST(MyConsta.URL_SYNC_CONTACTS)
    Call<ContactsMaster> SyncContacts(@Field("apikey") String apikey,
                                      @Field("pid") int pid);


    @FormUrlEncoded
    @POST(MyConsta.URL_SYNC_MESSAGES)
    Call<ChatMessageMaster> SyncChats(@Field("apikey") String apikey,
                                      @Field("user_id") int pid,
                                      @Field("timestamp") String timestamp);


    @FormUrlEncoded
    @POST(MyConsta.URL_SEND_MESSAGE)
    Call<ChatMessageMaster> SendMsg(@Field("apikey") String apikey,
                                    @Field("user_id") int pid,
                                    @Field("chatuser_id") long chatuser_id,
                                    @Field("type") int msgOrImage,
                                    @Field("user_type") int user_type,
                                    @Field("msg_data") String msgString);


    @GET
    Call<Steps> FitbitGetSteps(@Header("Authorization") String Authorization, @Url String StepsUrl);


  *//*  @Headers({"Accept-Encoding:gzip, deflate",
            "User-Agent:runscope/0.1",
           // "Accept:*//**//*
    //})*//*
    @GET
    Call<HeartRate> FitbitGetHeartRate(@Header("Authorization") String Authorization, @Url String HeartRateUrl);




    @FormUrlEncoded
    @POST(MyConsta.URL_EMERGENCY_CONTACTS)
    Call<SosContacts> SOSContacts(@Field("apikey") String apikey,
                                  @Field("pid") int pid);*/

}
